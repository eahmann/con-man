package com.awd.conman.controller;

import com.awd.conman.dto.request.LoginRequest;
import com.awd.conman.dto.request.RefreshTokenRequest;
import com.awd.conman.dto.request.RegisterRequest;
import com.awd.conman.dto.response.AuthenticationResponse;
import com.awd.conman.dto.response.MessageResponse;
import com.awd.conman.dao.UserDao;
import com.awd.conman.service.AuthService;
import com.awd.conman.service.RefreshTokenService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.OK;

@Slf4j
@RestController
@RequestMapping("/api/auth")
@AllArgsConstructor
public class AuthController {

    private final AuthService authService;
    private final UserDao userDao;
    private final RefreshTokenService refreshTokenService;

    @PostMapping("/signup")
    public ResponseEntity<MessageResponse> signup(@RequestBody RegisterRequest registerRequest) {
        if (userDao.existsByUsername(registerRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }
        if (userDao.existsByEmail(registerRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }
        authService.signup(registerRequest);
        return ResponseEntity.ok(new MessageResponse("User Registration Successful"));
    }

    @PostMapping("/login")
    public ResponseEntity<MessageResponse> login(@RequestBody LoginRequest loginRequest, HttpServletResponse res) {
        return authService.login(loginRequest, res);
    }

    @GetMapping("/verify/{token}")
    public ResponseEntity<MessageResponse> verifyAccount(@PathVariable String token) {
        authService.verifyAccount(token);
        return ResponseEntity.ok(new MessageResponse("Account Activated Successfully"));
    }

    @GetMapping("/refresh")
    public ResponseEntity<MessageResponse> refreshTokens(HttpServletResponse res, HttpServletRequest request) {
        return authService.refreshToken(res, request);
    }

/*    @PostMapping("/logout")
    public ResponseEntity<String> logout(@Valid @RequestBody RefreshTokenRequest refreshTokenRequest) {
        refreshTokenService.deleteRefreshToken(refreshTokenRequest.getRefreshToken());
        return ResponseEntity.status(OK).body("Refresh Token Deleted Successfully!!");
    }*/

    @GetMapping("/logout")
    public ResponseEntity<MessageResponse> logout(HttpServletResponse res) {
        return authService.logout(res);
    }

    @GetMapping("/all-cookies")
    public String readAllCookies(HttpServletRequest request) {

        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            return Arrays.stream(cookies)
                    .map(c -> c.getName() + "=" + c.getValue()).collect(Collectors.joining(", "));
        }

        return "No cookies";
    }
}
