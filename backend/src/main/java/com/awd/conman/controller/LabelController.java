package com.awd.conman.controller;

import com.awd.conman.dto.LabelDto;
import com.awd.conman.entity.enums.ELabelType;
import com.awd.conman.service.LabelService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/labels")
@AllArgsConstructor
public class LabelController {

    private final LabelService labelService;

    @GetMapping
    public List<LabelDto> getAllLabels() {
        return labelService.getAll();
    }

    @GetMapping("/{labelType}")
    public List<LabelDto> getAllByLabelType(@PathVariable ELabelType labelType) {
        return labelService.getAllByLabelType(labelType);
    }

    @PostMapping
    public LabelDto create(@RequestBody @Valid LabelDto labelDto) {
        return labelService.save(labelDto);
    }
}
