package com.awd.conman.controller;

import com.awd.conman.dto.ProjectDto;
import com.awd.conman.dto.TimeSegmentDto;
import com.awd.conman.service.ProjectService;
import com.awd.conman.service.TimeSegmentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/time")
@AllArgsConstructor
public class TimeSegmentController {

    private final ProjectService projectService;
    private final TimeSegmentService timeSegmentService;

    @GetMapping
    public List<TimeSegmentDto> getAllTimeSegments() {
        return timeSegmentService.getAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProjectDto> getProject(@PathVariable String id) {
        ProjectDto projectDto = projectService.getProject(id);
        return ResponseEntity.ok(projectDto);
    }

    @PostMapping("/{projectId}")
    public ResponseEntity<TimeSegmentDto> create(@PathVariable String projectId, @RequestBody @Valid TimeSegmentDto timeSegmentDto) {
        timeSegmentService.save(projectId, timeSegmentDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(timeSegmentDto);
    }
}
