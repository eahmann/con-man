package com.awd.conman.controller;

import com.awd.conman.dto.ProjectDto;
import com.awd.conman.dto.RoleDto;
import com.awd.conman.service.RoleService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/roles")
@AllArgsConstructor
public class RoleController {

    private final RoleService roleService;

    @GetMapping
    public List<RoleDto> getAllRoles() {
        return roleService.getAll();
    }

    @PostMapping()
    public ResponseEntity<RoleDto> create(@RequestBody @Valid RoleDto roleDto) {
        roleService.save(roleDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(roleDto);
    }

}
