package com.awd.conman.controller;

import com.awd.conman.dto.ProjectDto;
import com.awd.conman.dto.ProjectPageDto;
import com.awd.conman.dto.TimeSegmentPageDto;
import com.awd.conman.service.ProjectService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/projects")
@AllArgsConstructor
@Slf4j
public class ProjectController {

  private final ProjectService projectService;

  // Get all (paginated)
  @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
  @GetMapping
  public ResponseEntity<ProjectPageDto> getAllProjects(
          @RequestParam(value = "page", defaultValue = "0") Integer page,
          @RequestParam(value = "size", defaultValue = "2") Integer size,
          @RequestParam(value = "sort", defaultValue = "id,desc") String[] sort) {
    return projectService.getAll(page, size, sort);
  }

  // Get by ID
  @GetMapping("/{id}")
  @Operation(operationId = "getProject")
  public ResponseEntity<ProjectDto> getProject(@PathVariable String id) {
    ProjectDto projectDto = projectService.getProject(id);
    return ResponseEntity.ok(projectDto);
  }

  // Create
  @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
  @PostMapping("")
  public ResponseEntity<ProjectDto> create(@RequestBody @Valid ProjectDto projectDto) {
    return projectService.save(projectDto);
  }

  // Update
  @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
  @PutMapping("/{id}")
  public ResponseEntity<ProjectDto> update(@PathVariable String id, @RequestBody @Valid ProjectDto projectDto) {
    return projectService.update(id, projectDto);
  }

  // Get time segments for project by ID (paginated)
  @GetMapping("/{id}/time")
  public ResponseEntity<TimeSegmentPageDto> getProjectTimeSegments(
          @RequestParam(value = "page", defaultValue = "0") Integer page,
          @RequestParam(value = "size", defaultValue = "2") Integer size,
          @RequestParam(value = "sort", defaultValue = "id,desc") String[] sort,
          @PathVariable String id) {
    return projectService.getProjectTimeSegments(page, size, sort, id);
  }
}
