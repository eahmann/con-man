package com.awd.conman.controller;

import com.awd.conman.dto.ClientDto;
import com.awd.conman.service.ClientService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/clients")
@AllArgsConstructor
public class ClientController {
    private final ClientService clientService;

    @GetMapping
    public List<ClientDto> getAllClients() {
        return clientService.getAll();
    }

    @PostMapping
    public ClientDto create(@RequestBody @Valid ClientDto clientDto) {
        return clientService.save(clientDto);
    }
}
