package com.awd.conman.controller;

import com.awd.conman.dto.UserDto;
import com.awd.conman.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping
    public List<UserDto> getAllUsers() { return userService.getAll(); }

    @GetMapping("/{id}")
    public UserDto getUser(@PathVariable String id) {
        return userService.getUser(id);
    }

    @GetMapping("/self")
    public UserDto getSelf() { return userService.getSelf(); }

    @PostMapping
    public UserDto create(@RequestBody @Valid UserDto userDto) {
        return userService.save(userDto);
    }

    @PutMapping("/{userId}")
    public UserDto update(@PathVariable String userId, @RequestBody UserDto userDto) {
        return userService.update(userId, userDto);
    }

    // TODO: User query params to allow for multiple roles to be selected and filtered by
    @GetMapping("/role/{role}")
    public List<UserDto> getUsersByRole(@PathVariable String role) {
        return userService.getUsersByRole(role);
    }
}
