package com.awd.conman.controller;

import com.awd.conman.dto.LocationDto;
import com.awd.conman.service.LocationService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/locations")
@AllArgsConstructor
public class LocationController {


    private final LocationService locationService;

    @GetMapping
    public List<LocationDto> getAllLocations() {
        return locationService.getAll();
    }

    @GetMapping("/label/{label}")
    public List<LocationDto> findByLabel(@PathVariable String label) {
        return locationService.findByLabel(label);
    }

    @GetMapping("/{locationId}")
    public LocationDto getLocation(@PathVariable String locationId) {
        return locationService.getLocation(locationId);
    }

    @PostMapping
    public LocationDto create(@RequestBody @Valid LocationDto locationDto) {
        return locationService.save(locationDto);
    }
}
