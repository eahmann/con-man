package com.awd.conman.controller;

import com.awd.conman.dto.ProfileDto;
import com.awd.conman.service.ProfileService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/profiles")
@AllArgsConstructor
public class ProfileController {

    private final ProfileService profileService;

    @GetMapping
    public List<ProfileDto> getAllProfiles() {
        return profileService.getAll();
    }

    @GetMapping("/{id}")
    public ProfileDto getProfile(@PathVariable String id) {
        return profileService.getProfile(id);
    }

    @PostMapping
    public ProfileDto create(@RequestBody @Valid ProfileDto profileDto) {
        return profileService.save(profileDto);
    }

    @PutMapping("/{profileId}")
    public ProfileDto updateProfile(@PathVariable String profileId, @RequestBody @Valid ProfileDto profileDto) {
        return profileService.updateProfile(profileId, profileDto);
    }
}
