package com.awd.conman.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {
    public static final String ACTIVATION_EMAIL = "http://localhost:8080/api/auth/verify";
    public static final String AUTHORITIES_KEY = "scopes";
}
