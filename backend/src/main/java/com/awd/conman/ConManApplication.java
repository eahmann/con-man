package com.awd.conman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class ConManApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConManApplication.class, args);
    }

}
