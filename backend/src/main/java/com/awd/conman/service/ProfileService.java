package com.awd.conman.service;


import com.awd.conman.dao.ProfileDao;
import com.awd.conman.dto.ProfileDto;
import com.awd.conman.entity.Profile;
import com.awd.conman.exception.ConManException;
import com.awd.conman.mapper.ProfileMapper;
import com.awd.conman.mapper.ProjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class ProfileService {

    private final ProfileDao profileDao;
    private final ProfileMapper profileMapper;
    private final ProjectMapper projectMapper;

    @Transactional(readOnly = true)
    public List<ProfileDto> getAll() {
        return profileDao.findAll()
                .stream()
                .map(profileMapper::mapProfileToDto)
                .collect(toList());
    }

    @Transactional
    public ProfileDto save(ProfileDto profileDto) {
        profileDto.setCreatedDate(LocalDateTime.now());
        Profile profile = profileDao.save(profileMapper.mapDtoToProfile(profileDto));
        profileDto.setId(profile.getId());
        return profileDto;
    }

    @Transactional(readOnly = true)
    public ProfileDto getProfile(String id) {
        Profile profile = profileDao.findById(id)
                .orElseThrow(() -> new ConManException("Profile not found with id -" + id));
        return profileMapper.mapProfileToDto(profile);
    }

    @Transactional
    public ProfileDto updateProfile(String profileId, ProfileDto profileDto) {
        Profile profile = profileDao.findById(profileId)
                .orElseThrow(() -> new ConManException("Profile not found with id -" + profileId));
        profileDto.setId(profileId);
        Profile updateProfile = profileDao.save(profileMapper.mapDtoToProfile(profileDto));
        return profileMapper.mapProfileToDto(updateProfile);
    }
}
