package com.awd.conman.service;

import com.awd.conman.dao.ProfileDao;
import com.awd.conman.dao.RoleDao;
import com.awd.conman.dao.UserDao;
import com.awd.conman.dto.UserDto;
import com.awd.conman.entity.Profile;
import com.awd.conman.entity.Role;
import com.awd.conman.entity.User;
import com.awd.conman.exception.ConManException;
import com.awd.conman.mapper.ProfileMapper;
import com.awd.conman.mapper.UserMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class UserService {

  private final AuthService authService;
  private final UserDao userDao;
  private final ProfileDao profileDao;
  private final RoleDao roleDao;

  @Autowired private final UserMapper userMapper;

  @Autowired private final ProfileMapper profileMapper;

  @Transactional(readOnly = true)
  public List<UserDto> getAll() {
    return userDao.findAll().stream().map(userMapper::mapUserToDto).collect(toList());
  }

  @Transactional
  public UserDto save(UserDto userDto) {
    Profile profile = profileDao.save(new Profile());
    userDto.setProfile(profileMapper.mapProfileToDto(profile));
    User user = userDao.save(userMapper.mapDtoToUser(userDto));
    userDto.setId(user.getId());
    return userDto;
  }

  @Transactional
  public UserDto update(String userId, UserDto userDto) {
    User user =
            userDao
                    .findById(userId)
                    .orElseThrow(() -> new ConManException("User not found with id:" + userId));
    userDto.setId(userId);

    if (userDto.getProfile() != null) {
      Profile profile = profileDao.findById(user.getProfile().getId()).get();
      profileDao.save(profileMapper.mapDtoToProfile(userDto.getProfile(), profile));
    }

    User updatedUser = userDao.save(userMapper.mapDtoToUser(userDto, user));
    return userMapper.mapUserToDto(updatedUser);
  }

  @Transactional(readOnly = true)
  public UserDto getUser(String userId) {
    User user =
        userDao
            .findById(userId)
            .orElseThrow(() -> new ConManException("User not found with id:" + userId));
    return userMapper.mapUserToDto(user);
  }

  @Transactional(readOnly = true)
  public UserDto getSelf() {
    User user =
        userDao
            .findById(authService.getCurrentUser().getId())
            .orElseThrow(() -> new ConManException("Couldn't find yourself, that's weird"));
    return userMapper.mapUserToDto(user);
  }

  @Transactional(readOnly = true)
  public List<UserDto> getUsersByRole(String role) {
    Role role1;
    role1 = roleDao.findByName(role.toUpperCase(Locale.ROOT));
    if (role1 == null) {
      throw new ConManException("Cannot find role");
    }
    return userDao.getUsersByRolesContains(role1).stream()
        .map(userMapper::mapUserToDto)
        .collect(toList());
  }
}
