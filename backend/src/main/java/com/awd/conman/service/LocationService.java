package com.awd.conman.service;

import com.awd.conman.dao.LocationDao;
import com.awd.conman.dto.LocationDto;
import com.awd.conman.entity.Location;
import com.awd.conman.exception.ConManException;
import com.awd.conman.mapper.LocationMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class LocationService {

    private final LocationDao locationDao;
    private final LocationMapper locationMapper;


    @Transactional(readOnly = true)
    public List<LocationDto> getAll() {
        return locationDao.findAll()
                .stream()
                .map(locationMapper::mapLocationToDto)
                .collect(toList());
    }

    @Transactional(readOnly = true)
    public List<LocationDto> findByLabel(String label) {
        return locationDao.findByLabelsNameIgnoreCase(label)
                .stream()
                .map(locationMapper::mapLocationToDto)
                .collect(toList());
    }

    @Transactional
    public LocationDto save(LocationDto locationDto) {
        locationDto.setCreatedDate(LocalDateTime.now());
        Location location = locationDao.save(locationMapper.mapDtoToLocation(locationDto));
        locationDto.setId(location.getId());
        return locationDto;
    }

    @Transactional(readOnly = true)
    public LocationDto getLocation(String id) {
        Location location = locationDao.findById(id)
                .orElseThrow(() -> new ConManException("Location not found with id -" + id));
        return locationMapper.mapLocationToDto(location);
    }
}
