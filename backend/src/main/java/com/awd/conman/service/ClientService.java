package com.awd.conman.service;

import com.awd.conman.dao.ClientDao;
import com.awd.conman.dao.ProfileDao;
import com.awd.conman.dto.ClientDto;
import com.awd.conman.entity.Client;
import com.awd.conman.entity.Profile;
import com.awd.conman.mapper.ClientMapper;
import com.awd.conman.mapper.ProfileMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class ClientService {
    private final ClientDao clientDao;
    private final ClientMapper clientMapper;
    private final ProfileDao profileDao;
    private final ProfileMapper profileMapper;

    @Transactional(readOnly = true)
    public List<ClientDto> getAll() {
        return clientDao.findAll()
                .stream()
                .map(clientMapper::mapClientToDto)
                .collect(toList());
    }

    @Transactional
    public ClientDto save(ClientDto clientDto) {
        clientDto.setCreatedDate(LocalDateTime.now());

        // Get or create profile
        Profile profile;
        if (!clientDto.getProfile().getId().equals("undefined")) {
            profile = profileDao.findById(clientDto.getProfile().getId()).orElseThrow();
        }
        else {
            profile = profileDao.save(profileMapper.mapDtoToProfile(clientDto.getProfile()));
        }

        clientDto.getProfile().setId(profile.getId());
        Client client = clientDao.save(clientMapper.mapDtoToClient(clientDto));
        clientDto.setId(client.getId());
        return clientDto;
    }
}
