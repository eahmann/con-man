package com.awd.conman.service;

import com.awd.conman.dao.RoleDao;
import com.awd.conman.dto.RoleDto;
import com.awd.conman.dto.TimeSegmentDto;
import com.awd.conman.dto.UserDto;
import com.awd.conman.entity.Project;
import com.awd.conman.entity.Role;
import com.awd.conman.entity.TimeSegment;
import com.awd.conman.mapper.RoleMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class RoleService {

    private final RoleDao roleDao;
    private final RoleMapper roleMapper;
    private final AuthService authService;

    @Transactional(readOnly = true)
    public List<RoleDto> getAll() {
        return roleDao.findAll()
                .stream()
                .map(roleMapper::mapRoleToDto)
                .collect(toList());
    }

    @Transactional
    public RoleDto save(RoleDto roleDto) {
        Role role = roleDao.save(roleMapper.mapDtoToRole(roleDto));
        roleDto.setCreatedDate(role.getCreatedDate());
        roleDto.setId(role.getId());
        return roleDto;
    }
}
