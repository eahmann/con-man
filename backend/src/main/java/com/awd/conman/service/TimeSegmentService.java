package com.awd.conman.service;

import com.awd.conman.dao.ProjectDao;
import com.awd.conman.dao.TimeSegmentDao;
import com.awd.conman.dto.TimeSegmentDto;
import com.awd.conman.dto.TimeSegmentPageDto;
import com.awd.conman.entity.Project;
import com.awd.conman.entity.TimeSegment;
import com.awd.conman.mapper.TimeSegmentMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class TimeSegmentService {

  private final TimeSegmentDao timeSegmentDao;
  private final ProjectDao projectDao;
  private final TimeSegmentMapper timeSegmentMapper;
  private final AuthService authService;

  private Sort.Direction getSortDirection(String direction) {
    if (direction.equals("asc")) {
      return Sort.Direction.ASC;
    } else if (direction.equals("desc")) {
      return Sort.Direction.DESC;
    }

    return Sort.Direction.ASC;
  }

  @Transactional(readOnly = true)
  public List<TimeSegmentDto> getAll() {
    return timeSegmentDao.findAll().stream()
            .map(timeSegmentMapper::mapTimeSegmentToDto)
            .collect(toList());
  }

  @Transactional
  public TimeSegmentDto save(String projectId, TimeSegmentDto timeSegmentDto) {
    timeSegmentDto.setCreatedBy(authService.getCurrentUser().getId());
    timeSegmentDto.setProjectId(projectId);
    Project project = projectDao.findById(projectId).orElseThrow();
    TimeSegment timeSegment =
            timeSegmentDao.save(timeSegmentMapper.mapDtoToTimeSegment(timeSegmentDto));
    project.getTimeSegments().add(timeSegment);

    // Update total hours on project
    Set<TimeSegment> timeSegments = project.getTimeSegments();
    project.setTotalHours(timeSegments.stream().mapToDouble(TimeSegment::getHours).sum());

    timeSegmentDto.setCreatedDate(timeSegment.getCreatedDate());
    timeSegmentDto.setId(timeSegment.getId());

    return timeSegmentDto;
  }

  @Transactional(readOnly = true)
  public ResponseEntity<TimeSegmentPageDto> getAllByProjectId(
          Integer page, Integer size, String[] sort, String id) {

    List<TimeSegmentDto> timeSegments;

    try {
      List<Order> orders = new ArrayList<Order>();

      if (sort[0].contains(",")) {
        // will sort more than 2 fields
        // sortOrder="field, direction"
        for (String sortOrder : sort) {
          String[] _sort = sortOrder.split(",");
          orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
        }
      } else {
        // sort=[field, direction]
        orders.add(new Order(getSortDirection(sort[1]), sort[0]));
      }

      Pageable paging = PageRequest.of(page, size, Sort.by(orders));
      Page<TimeSegmentDto> timeSegmentPage =
              timeSegmentDao
                      .findTimeSegmentsByProjectId(paging, id)
                      .map(timeSegmentMapper::mapTimeSegmentToDto);
      timeSegments = timeSegmentPage.getContent();

      TimeSegmentPageDto response =
              TimeSegmentPageDto.builder()
                      .items(timeSegments)
                      .currentPage(timeSegmentPage.getNumber())
                      .totalItems(timeSegmentPage.getTotalElements())
                      .totalPages(timeSegmentPage.getTotalPages())
                      .sortBy(timeSegmentPage.getSort().toString())
                      .build();

      return ResponseEntity.ok(response);
    } catch (Exception e) {
      throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.toString());
    }
  }
}
