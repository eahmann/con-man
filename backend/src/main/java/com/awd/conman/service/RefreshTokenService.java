package com.awd.conman.service;

import com.awd.conman.exception.ConManException;
import com.awd.conman.entity.RefreshToken;
import com.awd.conman.dao.RefreshTokenDao;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.util.UUID;

@Service
@AllArgsConstructor
@Transactional
public class RefreshTokenService {

  private final RefreshTokenDao refreshTokenDao;

  RefreshToken generateRefreshToken() {
    RefreshToken refreshToken = new RefreshToken();
    refreshToken.setToken(UUID.randomUUID().toString());
    refreshToken.setCreatedDate(Instant.now());
    return refreshTokenDao.save(refreshToken);
  }



  public void cycleRefreshToken(HttpServletRequest req) {
    validateRefreshToken(req);
    deleteRefreshToken(req);
  }

  void validateRefreshToken(HttpServletRequest req) {
    refreshTokenDao
            .findByToken(extractCookie(req))
            .orElseThrow(() -> new ConManException("Invalid refresh Token"));
  }

  public void deleteRefreshToken(HttpServletRequest req) {
    refreshTokenDao.deleteByToken(extractCookie(req));
  }

  private String extractCookie(HttpServletRequest req) {
    for (Cookie c : req.getCookies()) {
      if (c.getName().equals("X-Refresh-Token")) return c.getValue();
    }
    return null;
  }
}
