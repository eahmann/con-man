package com.awd.conman.service;

import com.awd.conman.dao.ProjectDao;
import com.awd.conman.dao.TimeSegmentDao;
import com.awd.conman.dto.ProjectDto;
import com.awd.conman.dto.ProjectPageDto;
import com.awd.conman.dto.TimeSegmentDto;
import com.awd.conman.dto.TimeSegmentPageDto;
import com.awd.conman.entity.Project;
import com.awd.conman.exception.ConManException;
import com.awd.conman.mapper.ProfileMapper;
import com.awd.conman.mapper.ProjectMapper;
import com.awd.conman.mapper.TimeSegmentMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class ProjectService {

  private final ProjectDao projectDao;
  private final TimeSegmentDao timeSegmentDao;
  private final ProjectMapper projectMapper;
  private final ProfileMapper profileMapper;
  private final TimeSegmentMapper timeSegmentMapper;
  private final AuthService authService;

  private Sort.Direction getSortDirection(String direction) {
    if (direction.equals("asc")) {
      return Sort.Direction.ASC;
    } else if (direction.equals("desc")) {
      return Sort.Direction.DESC;
    }

    return Sort.Direction.ASC;
  }

  @Transactional(readOnly = true)
  public ResponseEntity<ProjectPageDto> getAll(Integer page, Integer size, String[] sort) {

    List<ProjectDto> projects;

    try {
      List<Order> orders = new ArrayList<Order>();

      if (sort[0].contains(",")) {
        // will sort more than 2 fields
        // sortOrder="field, direction"
        for (String sortOrder : sort) {
          String[] _sort = sortOrder.split(",");
          orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
        }
      } else {
        // sort=[field, direction]
        orders.add(new Order(getSortDirection(sort[1]), sort[0]));
      }

      Pageable paging = PageRequest.of(page, size, Sort.by(orders));
      Page<ProjectDto> projectPage = projectDao.findAll(paging).map(projectMapper::mapProjectToDto);
      projects = projectPage.getContent();

      ProjectPageDto response =
          ProjectPageDto.builder()
              .items(projects)
              .currentPage(projectPage.getNumber())
              .totalItems(projectPage.getTotalElements())
              .totalPages(projectPage.getTotalPages())
              .sortBy(projectPage.getSort().toString())
              .build();

      return ResponseEntity.ok(response);
    } catch (Exception e) {
      throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.toString());
    }
  }

  @Transactional
  public ProjectDto save(String profileId, ProjectDto projectDto) {
    projectDto.setCreatedById(authService.getCurrentUser().getId());
    Project project = projectDao.save(projectMapper.mapDtoToProject(projectDto));
    projectDto.setCreatedDate(project.getCreatedDate());
    projectDto.setId(project.getId());

    return projectDto;
  }

  @Transactional
  public ResponseEntity<ProjectDto> save(ProjectDto projectDto) {
    try {
      projectDto.setCreatedById(authService.getCurrentUser().getId());
      Project project = projectDao.save(projectMapper.mapDtoToProject(projectDto));
      projectDto.setCreatedDate(project.getCreatedDate());
      projectDto = projectMapper.mapProjectToDto(project);

      return ResponseEntity.ok(projectDto);
    } catch (Exception e) {
      throw new ResponseStatusException(
              HttpStatus.INTERNAL_SERVER_ERROR, "Invalid request. Check request body");
    }
  }

  @Transactional(readOnly = true)
  public ProjectDto getProject(String id) {
    Project project =
        projectDao
            .findById(id)
            .orElseThrow(() -> new ConManException("Project not found with id -" + id));
    return projectMapper.mapProjectToDto(project);
  }

  @Transactional(readOnly = true)
  public ResponseEntity<TimeSegmentPageDto> getProjectTimeSegments(
          Integer page, Integer size, String[] sort, String id) {

    List<TimeSegmentDto> timeSegments;

    try {
      List<Order> orders = new ArrayList<Order>();

      if (sort[0].contains(",")) {
        // will sort more than 2 fields
        // sortOrder="field, direction"
        for (String sortOrder : sort) {
          String[] _sort = sortOrder.split(",");
          orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
        }
      } else {
        // sort=[field, direction]
        orders.add(new Order(getSortDirection(sort[1]), sort[0]));
      }

      Pageable paging = PageRequest.of(page, size, Sort.by(orders));
      Page<TimeSegmentDto> projectPage =
              timeSegmentDao
                      .findTimeSegmentsByProjectId(paging, id)
                      .map(timeSegmentMapper::mapTimeSegmentToDto);
      timeSegments = projectPage.getContent();

      TimeSegmentPageDto response =
              TimeSegmentPageDto.builder()
                      .items(timeSegments)
                      .currentPage(projectPage.getNumber())
                      .totalItems(projectPage.getTotalElements())
                      .totalPages(projectPage.getTotalPages())
                      .sortBy(projectPage.getSort().toString())
                      .build();

      return ResponseEntity.ok(response);
    } catch (Exception e) {
      throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.toString());
    }
  }

  @Transactional
  public ResponseEntity<ProjectDto> update(String id, ProjectDto projectDto) {
    Project project =
            projectDao
                    .findById(id)
                    .orElseThrow(
                            () -> new ConManException("Project not found with id:" + projectDto.getId()));

    Project updatedProject = projectDao.save(projectMapper.mapDtoToProject(projectDto, project));
    return ResponseEntity.ok(projectMapper.mapProjectToDto(updatedProject));
  }
}
