package com.awd.conman.service;

import com.awd.conman.dao.LabelDao;
import com.awd.conman.dto.LabelDto;
import com.awd.conman.entity.Label;
import com.awd.conman.entity.enums.ELabelType;
import com.awd.conman.mapper.LabelMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class LabelService {
    private final LabelDao labelDao;
    private final LabelMapper labelMapper;

    @Transactional(readOnly = true)
    public List<LabelDto> getAll() {
        return labelDao.findAll()
                .stream()
                .map(labelMapper::mapLabelToDto)
                .collect(toList());
    }

    @Transactional(readOnly = true)
    public List<LabelDto> getAllByLabelType(ELabelType labelType) {
        return labelDao.getAllByLabelType(labelType)
                .stream()
                .map(labelMapper::mapLabelToDto)
                .collect(toList());
    }

    @Transactional
    public LabelDto save(LabelDto labelDto) {
        labelDto.setCreatedDate(LocalDateTime.now());
        Label label = labelDao.save(labelMapper.mapDtoToLabel(labelDto));
        labelDto.setId(label.getId());
        return labelDto;
    }

}
