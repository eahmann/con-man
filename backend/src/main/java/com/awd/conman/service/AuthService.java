package com.awd.conman.service;

import com.awd.conman.dao.ProfileDao;
import com.awd.conman.dto.request.LoginRequest;
import com.awd.conman.dto.request.RegisterRequest;
import com.awd.conman.dto.response.MessageResponse;
import com.awd.conman.entity.*;
import com.awd.conman.entity.enums.EProfileType;
import com.awd.conman.exception.ConManException;
import com.awd.conman.dao.UserDao;
import com.awd.conman.dao.VerificationTokenDao;
import com.awd.conman.security.JwtProvider;
import io.jsonwebtoken.Claims;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;
import java.util.UUID;

import static com.awd.conman.util.Constants.ACTIVATION_EMAIL;
import static java.time.Instant.now;

@Service
@AllArgsConstructor
@Slf4j
public class AuthService {

  private final UserDao userDao;
  private final ProfileDao profileDao;
  private final PasswordEncoder passwordEncoder;
  private final AuthenticationManager authenticationManager;
  private final JwtProvider jwtProvider;
  private final MailContentBuilder mailContentBuilder;
  private final MailService mailService;
  private final VerificationTokenDao verificationTokenDao;
  private final RefreshTokenService refreshTokenService;

  @Transactional
  public void signup(RegisterRequest registerRequest) {
    User user = new User();
    user.setUsername(registerRequest.getUsername());
    user.setEmail(registerRequest.getEmail());
    user.setPassword(encodePassword(registerRequest.getPassword()));
    user.setEnabled(false);
    Profile profile = profileDao.save(new Profile());
    profile.setProfileType(EProfileType.USER);
    user.setProfile(profile);

    userDao.save(user);
    String token = generateVerificationToken(user);
    String message =
        mailContentBuilder.build(
            "Thank you for signing up to HCI - AWD Contractor Manager, please click on the below url to activate your account : "
                + ACTIVATION_EMAIL
                + "/"
                + token);

    mailService.sendMail(
        new NotificationEmail("Please Activate your account", user.getEmail(), message));
  }

  @Transactional(readOnly = true)
  User getCurrentUser() {
    org.springframework.security.core.userdetails.User principal =
        (org.springframework.security.core.userdetails.User)
            SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    return userDao
        .findByUsername(principal.getUsername())
        .orElseThrow(
            () ->
                new UsernameNotFoundException("User name not found - " + principal.getUsername()));
  }

  private String generateVerificationToken(User user) {
    String token = UUID.randomUUID().toString();
    VerificationToken verificationToken = new VerificationToken();
    verificationToken.setToken(token);
    verificationToken.setUser(user);
    verificationTokenDao.save(verificationToken);
    return token;
  }

  private String encodePassword(String password) {
    return passwordEncoder.encode(password);
  }

  /*    public AuthenticationResponse login(LoginRequest loginRequest) {
      Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),
              loginRequest.getPassword()));
      SecurityContextHolder.getContext().setAuthentication(authenticate);
      String token = jwtProvider.generateToken(authenticate);
      return AuthenticationResponse.builder()
              .authenticationToken(token)
              .refreshToken(refreshTokenService.generateRefreshToken().getToken())
              .expiresAt(Instant.now().plusMillis(jwtProvider.getJwtExpirationInMillis()))
              .username(loginRequest.getUsername())
              .build();
  }*/

  public ResponseEntity<MessageResponse> login(LoginRequest loginRequest, HttpServletResponse res) {
    Authentication authenticate =
        authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(
                loginRequest.getUsername(), loginRequest.getPassword()));
    SecurityContextHolder.getContext().setAuthentication(authenticate);
    MessageResponse messageResponse = new MessageResponse("Logged in");

    generateJwtCookie(res, authenticate);
    generateRefreshCookie(res);

    return ResponseEntity.ok().body(messageResponse);
  }

  public ResponseEntity<MessageResponse> logout(HttpServletResponse res) {
    MessageResponse messageResponse = new MessageResponse("Logged out");
    Cookie cookie = new Cookie("X-Access-Token", null);
    cookie.setMaxAge(0);
    cookie.setPath("/");
    cookie.setSecure(false);
    cookie.setHttpOnly(true);
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.addCookie(cookie);

    Cookie refreshToken = new Cookie("X-Refresh-Token", null);
    refreshToken.setPath("/");
    refreshToken.setMaxAge(0);
    refreshToken.setSecure(false);
    refreshToken.setHttpOnly(true);
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.addCookie(refreshToken);
    return ResponseEntity.ok().body(messageResponse);
  }

  /*    public AuthenticationResponse refreshToken(RefreshTokenRequest refreshTokenRequest) {
      refreshTokenService.validateRefreshToken(refreshTokenRequest.getRefreshToken());
      String token = jwtProvider.generateTokenWithUserName(refreshTokenRequest.getUsername());
      return AuthenticationResponse.builder()
              .authenticationToken(token)
              .refreshToken(refreshTokenRequest.getRefreshToken())
              .expiresAt(Instant.now().plusMillis(jwtProvider.getJwtExpirationInMillis()))
              .username(refreshTokenRequest.getUsername())
              .build();
  }*/

  public ResponseEntity<MessageResponse> refreshToken(
      HttpServletResponse res, HttpServletRequest req) {
    MessageResponse messageResponse = new MessageResponse("Token refresh success");

    Claims claims = (Claims) req.getAttribute("claims");
    String token = jwtProvider.generateTokenWithClaims(claims);
    generateJwtCookie(res, token);
    refreshTokenService.cycleRefreshToken(req);
    generateRefreshCookie(res);
    return ResponseEntity.ok().body(messageResponse);
  }

  public void verifyAccount(String token) {
    Optional<VerificationToken> verificationTokenOptional = verificationTokenDao.findByToken(token);
    fetchUserAndEnable(
        verificationTokenOptional.orElseThrow(() -> new ConManException("Invalid Token")));
  }

  @Transactional
  void fetchUserAndEnable(VerificationToken verificationToken) {
    String username = verificationToken.getUser().getUsername();
    User user =
        userDao
            .findByUsername(username)
            .orElseThrow(() -> new ConManException("User Not Found with id - " + username));
    user.setEnabled(true);
    userDao.save(user);
  }

  public void generateJwtCookie(HttpServletResponse res, Authentication authentication) {
    String token = jwtProvider.generateToken(authentication);
    Cookie bearerToken = new Cookie("X-Access-Token", token);
    bearerToken.setPath("/");
    // Cookie max age, actual token expires sooner
    bearerToken.setMaxAge(5256000);
    bearerToken.setSecure(false);
    bearerToken.setHttpOnly(true);
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.addCookie(bearerToken);
  }

  public void generateJwtCookie(HttpServletResponse res, String token) {
    Cookie bearerToken = new Cookie("X-Access-Token", token);
    bearerToken.setPath("/");
    // Cookie max age, actual token expires sooner
    bearerToken.setMaxAge(5256000);
    bearerToken.setSecure(false);
    bearerToken.setHttpOnly(true);
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.addCookie(bearerToken);
  }

  public void generateRefreshCookie(HttpServletResponse res) {
    RefreshToken refresh_token = refreshTokenService.generateRefreshToken();
    Cookie refreshToken = new Cookie("X-Refresh-Token", refresh_token.getToken());
    refreshToken.setPath("/");
    refreshToken.setMaxAge(5256000);
    refreshToken.setSecure(false);
    refreshToken.setHttpOnly(true);
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.addCookie(refreshToken);
  }
}
