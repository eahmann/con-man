package com.awd.conman.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@JsonPropertyOrder({"items", "totalItems", "currentPage", "totalPages", "sortBy"})
public class BasePageDto {
    private long totalItems;
    private long totalPages;
    private long currentPage;
    private String sortBy;
}
