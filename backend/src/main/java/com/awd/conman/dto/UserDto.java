package com.awd.conman.dto;

import com.awd.conman.entity.Profile;
import com.awd.conman.entity.Role;
import com.awd.conman.entity.TimeSegment;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;
import java.util.Set;

@SuperBuilder
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UserDto extends BaseDto {
    private String username;
    @JsonIgnore
    private String password;
    private String email;
    private Boolean enabled;
    private Set<RoleDto> roles;
    private ProfileDto profile;
}
