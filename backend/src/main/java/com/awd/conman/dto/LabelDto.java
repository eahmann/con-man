package com.awd.conman.dto;

import com.awd.conman.entity.enums.ELabelType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class LabelDto extends BaseDto {
    private String name;
    private ELabelType labelType;
}
