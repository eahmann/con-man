package com.awd.conman.dto;

import com.awd.conman.entity.Project;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Set;

@SuperBuilder
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ClientDto extends BaseDto {
    private Set<ProjectDto> projects;
    private ProfileDto profile;
}
