package com.awd.conman.dto;

import com.awd.conman.entity.enums.EProfileType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Set;

@SuperBuilder
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ProfileDto extends BaseDto {
    private EProfileType profileType;
    private String firstName;
    private String lastName;
    private String businessName;
    private Set<LocationDto> locations;
}
