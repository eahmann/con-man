package com.awd.conman.dto;

import com.awd.conman.entity.Label;
import com.awd.conman.entity.Location;
import com.awd.conman.entity.enums.EProjectStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.OneToMany;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@SuperBuilder
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ProjectDto extends BaseDto {
    private String name;
    private String description;
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss", shape=JsonFormat.Shape.STRING)
    private Date startDate;
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss", shape=JsonFormat.Shape.STRING)
    private Date endDate;
    private EProjectStatus status;
    private String createdById;
    private ClientDto client;
    private Set<TimeSegmentDto> timeSegments;
    private Set<LabelDto> labels;
    private Double totalHours;
    private Set<LocationDto> locations;
}
