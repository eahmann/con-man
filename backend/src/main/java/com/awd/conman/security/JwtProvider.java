package com.awd.conman.security;

import com.awd.conman.exception.ConManException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import static io.jsonwebtoken.Jwts.parserBuilder;
import static java.util.Date.from;
import static com.awd.conman.util.Constants.AUTHORITIES_KEY;

@Service
public class JwtProvider {

  private KeyStore keyStore;

  @Value("${jwt.expiration.time}")
  private Long jwtExpirationInMillis;

  @PostConstruct
  public void init() {
    try {
      keyStore = KeyStore.getInstance("JKS");
      InputStream resourceAsStream = getClass().getResourceAsStream("/conman.jks");
      keyStore.load(resourceAsStream, "conman".toCharArray());
    } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e) {
      throw new ConManException("Exception occurred while loading keystore");
    }
  }

  public String generateToken(Authentication authentication) {
    org.springframework.security.core.userdetails.User principal =
        (User) authentication.getPrincipal();
    final String authorities =
        authentication.getAuthorities().stream()
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.joining(","));
    System.out.println(authentication.toString());
    return Jwts.builder()
        .setSubject(principal.getUsername())
        .claim(AUTHORITIES_KEY, authorities)
        .setIssuedAt(from(Instant.now()))
        .signWith(getPrivateKey())
        .setExpiration(Date.from(Instant.now().plusMillis(jwtExpirationInMillis)))
        .compact();
  }

  public String generateTokenWithUserName(String username) {
    return Jwts.builder()
        .setSubject(username)
        .setIssuedAt(from(Instant.now()))
        .signWith(getPrivateKey())
        .setExpiration(Date.from(Instant.now().plusMillis(jwtExpirationInMillis)))
        .compact();
  }

  public String generateTokenWithClaims(Claims claims) {
    return Jwts.builder()
            .setSubject(claims.getSubject())
            .claim(AUTHORITIES_KEY, claims.get("scopes"))
            .setIssuedAt(from(Instant.now()))
            .signWith(getPrivateKey())
            .setExpiration(Date.from(Instant.now().plusMillis(jwtExpirationInMillis)))
            .compact();
  }

  private PrivateKey getPrivateKey() {
    try {
      return (PrivateKey) keyStore.getKey("conman", "conman".toCharArray());
    } catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException e) {
      throw new ConManException(
          "Exception occurred while retrieving public key from keystore");
    }
  }

  public boolean validateToken(String jwt) {
    parserBuilder().setSigningKey(getPublickey()).build().parse(jwt);
    return true;
  }

  PublicKey getPublickey() {
    try {
      return keyStore.getCertificate("conman").getPublicKey();
    } catch (KeyStoreException e) {
      throw new ConManException(
          "Exception occurred while retrieving public key from keystore");
    }
  }

  public String getUsernameFromJWT(String token) {
    Claims claims =
        parserBuilder().setSigningKey(getPublickey()).build().parseClaimsJws(token).getBody();
    return claims.getSubject();
  }

  public Long getJwtExpirationInMillis() {
    return jwtExpirationInMillis;
  }

  UsernamePasswordAuthenticationToken getAuthentication(
      final String token, final Authentication existingAuth, final UserDetails userDetails) {
    final JwtParser jwtParser = Jwts.parserBuilder().setSigningKey(getPrivateKey()).build();
    final Jws<Claims> claimsJws = jwtParser.parseClaimsJws(token);
    final Claims claims = claimsJws.getBody();
    final Collection<SimpleGrantedAuthority> authorities =
        Arrays.stream(claims.get(AUTHORITIES_KEY).toString().split(","))
            .map(SimpleGrantedAuthority::new)
            .collect(Collectors.toList());
    return new UsernamePasswordAuthenticationToken(userDetails, "", authorities);
  }
}
