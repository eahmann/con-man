package com.awd.conman.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static io.jsonwebtoken.Jwts.parserBuilder;

@Component
@Slf4j
public class JwtAuthenticationFilter extends OncePerRequestFilter {

  @Autowired private JwtProvider jwtProvider;
  @Qualifier("userDetailsServiceImpl")
  @Autowired private UserDetailsService userDetailsService;

  @Override
  protected void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    try {
      String jwt = getJwtFromRequest(request);

      if (StringUtils.hasText(jwt) && jwtProvider.validateToken(jwt)) {
        String username = jwtProvider.getUsernameFromJWT(jwt);

        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken authentication =
            new UsernamePasswordAuthenticationToken(
                userDetails, null, userDetails.getAuthorities());
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String requestURL = request.getRequestURL().toString();
        if (requestURL.endsWith("auth/refresh")) {
          String token = getJwtFromRequest(request);
          Claims claims =
              parserBuilder()
                  .setSigningKey(jwtProvider.getPublickey())
                  .build()
                  .parseClaimsJws(token)
                  .getBody();
          allowForRefreshToken(claims, request);
        }
      }
    } catch (ExpiredJwtException ex) {
      String requestURL = request.getRequestURL().toString();
      if (requestURL.endsWith("refresh")) {
        allowForRefreshToken(ex.getClaims(), request);
      }
    } catch (BadCredentialsException ex) {
      request.setAttribute("exception", ex);
      response.sendError(500, "Bad credentials exception");
    } catch (Exception ex) {
      request.setAttribute("exception", ex);
      //response.sendError(500, "Unknown exception");
      log.info(ex.toString());
    }

    filterChain.doFilter(request, response);
  }

  private void allowForRefreshToken(Claims claims, HttpServletRequest request) {
    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
        new UsernamePasswordAuthenticationToken(null, null, null);
    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    request.setAttribute("claims", claims);
  }

  private String getJwtFromRequest(HttpServletRequest req) {
    if (req.getCookies() != null) {
      for (Cookie c : req.getCookies()) {
        if (c.getName().equals("X-Access-Token")) return c.getValue();
      }
    }
    return null;
  }
}
