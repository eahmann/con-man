package com.awd.conman.mapper;

import com.awd.conman.dto.TimeSegmentDto;
import com.awd.conman.entity.TimeSegment;
import com.awd.conman.entity.User;
import org.mapstruct.*;


@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN, uses = {UserMapper.class})
public interface TimeSegmentMapper {

    @Mapping(target = "createdBy", source = "createdBy.id")
    @Mapping(target = "employeeId", source = "employee.id")
    @Mapping(target = "projectId", source = "project.id")
    @Mapping(target = "employeeName", expression = "java(getName(timeSegment.employee))")
    TimeSegmentDto mapTimeSegmentToDto(TimeSegment timeSegment);

    default String getName(User employee) {
        if (employee.getProfile().getFirstName() != null) {
            return employee.getProfile().getFirstName() + " " + employee.getProfile().getLastName();
        }
        return "Unknown name";
    }

    @InheritInverseConfiguration
    TimeSegment mapDtoToTimeSegment(TimeSegmentDto timeSegmentDto);

    @InheritInverseConfiguration
    TimeSegment mapDtoToTimeSegment(TimeSegmentDto timeSegmentDto, @MappingTarget TimeSegment timeSegment);
}
