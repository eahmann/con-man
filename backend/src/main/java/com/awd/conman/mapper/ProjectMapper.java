package com.awd.conman.mapper;

import com.awd.conman.dto.ProjectDto;
import com.awd.conman.entity.Project;
import org.mapstruct.*;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.WARN,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        uses = {TimeSegmentMapper.class, ProfileMapper.class, ClientMapper.class})
public interface ProjectMapper {

  @Mapping(target = "createdById", source = "createdBy.id")
  ProjectDto mapProjectToDto(Project project);

  @InheritInverseConfiguration
  Project mapDtoToProject(ProjectDto project);

  @InheritInverseConfiguration
  Project mapDtoToProject(ProjectDto projectDto, @MappingTarget Project project);
}
