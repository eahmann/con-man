package com.awd.conman.mapper;

import com.awd.conman.dto.LabelDto;
import com.awd.conman.entity.Label;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN)
public interface LabelMapper {
    LabelDto mapLabelToDto(Label label);

    @InheritInverseConfiguration
    Label mapDtoToLabel(LabelDto labelDto);
}
