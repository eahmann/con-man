package com.awd.conman.mapper;

import com.awd.conman.dto.LocationDto;
import com.awd.conman.entity.Location;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN)
public interface LocationMapper {

    LocationDto mapLocationToDto(Location location);

    @InheritInverseConfiguration
    Location mapDtoToLocation(LocationDto locationDto);

    Location mapDtoToLocation(LocationDto locationDto, @MappingTarget Location location);

}
