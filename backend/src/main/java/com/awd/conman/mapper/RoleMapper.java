package com.awd.conman.mapper;

import com.awd.conman.dto.RoleDto;
import com.awd.conman.entity.Role;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN)
public interface RoleMapper {

    RoleDto mapRoleToDto(Role role);

    @InheritInverseConfiguration
    Role mapDtoToRole(RoleDto roleDto);
}
