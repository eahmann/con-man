package com.awd.conman.mapper;

import com.awd.conman.dto.UserDto;
import com.awd.conman.entity.User;
import org.mapstruct.*;

@Mapper(
    componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.WARN,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    uses = {ProfileMapper.class})
public interface UserMapper {

  UserDto mapUserToDto(User user);

  @InheritInverseConfiguration
  User mapDtoToUser(UserDto userDto);

  User mapDtoToUser(UserDto userDto, @MappingTarget User user);
}
