package com.awd.conman.mapper;

import com.awd.conman.dto.ClientDto;
import com.awd.conman.entity.Client;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN, uses = {ProjectMapper.class, TimeSegmentMapper.class, ProfileMapper.class})
public interface ClientMapper {

    ClientDto mapClientToDto(Client client);

    @InheritInverseConfiguration
    Client mapDtoToClient(ClientDto clientDto);
}
