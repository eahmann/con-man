package com.awd.conman.mapper;

import com.awd.conman.dto.ProfileDto;
import com.awd.conman.entity.Profile;
import org.mapstruct.*;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.WARN,
        uses = {LocationMapper.class},
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ProfileMapper {

  ProfileDto mapProfileToDto(Profile profile);

  @InheritInverseConfiguration
  Profile mapDtoToProfile(ProfileDto profileDto);

  Profile mapDtoToProfile(ProfileDto profileDto, @MappingTarget Profile profile);
}
