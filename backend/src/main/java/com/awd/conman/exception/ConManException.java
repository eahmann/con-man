package com.awd.conman.exception;

public class ConManException extends RuntimeException {
    public ConManException(String message) {
        super(message);
    }
}
