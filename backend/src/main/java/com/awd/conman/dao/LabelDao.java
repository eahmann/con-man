package com.awd.conman.dao;

import com.awd.conman.entity.Label;
import com.awd.conman.entity.enums.ELabelType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LabelDao extends JpaRepository<Label, String> {
    List<Label> getAllByLabelType(ELabelType labelType);
}
