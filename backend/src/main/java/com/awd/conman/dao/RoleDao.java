package com.awd.conman.dao;


import com.awd.conman.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleDao extends JpaRepository<Role, String> {
    Role findByName(String role);
}
