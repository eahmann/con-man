package com.awd.conman.dao;

import com.awd.conman.entity.Role;
import com.awd.conman.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserDao extends JpaRepository<User, String> {
    Optional<User> findByUsername(String username);
    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    List<User> getUsersByRolesContains(Role role);
}
