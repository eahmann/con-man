package com.awd.conman.dao;

import com.awd.conman.entity.TimeSegment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TimeSegmentDao extends JpaRepository<TimeSegment, Long> {

    Page<TimeSegment> findTimeSegmentsByProjectId(Pageable pageable, String id);
}
