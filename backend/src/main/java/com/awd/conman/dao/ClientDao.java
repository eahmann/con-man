package com.awd.conman.dao;

import com.awd.conman.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientDao extends JpaRepository<Client, String> {
}
