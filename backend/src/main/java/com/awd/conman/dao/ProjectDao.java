package com.awd.conman.dao;

import com.awd.conman.entity.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProjectDao extends JpaRepository<Project, String> {

    @Override
    Page<Project> findAll(Pageable pageable);

    Optional<Project> findById(String id);
    
    Optional<Project> findByClientId(String id);
}
