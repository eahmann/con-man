package com.awd.conman.dao;

import com.awd.conman.entity.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfileDao extends JpaRepository<Profile, String> {

}
