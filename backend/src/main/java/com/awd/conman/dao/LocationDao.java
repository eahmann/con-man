package com.awd.conman.dao;

import com.awd.conman.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LocationDao extends JpaRepository<Location, String> {
    List<Location> findByLabelsNameIgnoreCase(String label);
}
