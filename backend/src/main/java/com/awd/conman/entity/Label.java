package com.awd.conman.entity;

import com.awd.conman.entity.enums.ELabelType;
import com.awd.conman.entity.enums.EProjectStatus;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SuperBuilder
public class Label extends BaseEntity {
    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    private ELabelType labelType;
}
