package com.awd.conman.entity;

import com.awd.conman.entity.enums.EProfileType;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@SuperBuilder
@Setter
@Getter
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Profile extends BaseEntity {
    @Enumerated(EnumType.STRING)
    private EProfileType profileType;
    private String firstName;
    private String lastName;
    private String businessName;
    private String businessPhoneNumber;
    private String cellPhoneNumber;
    private String homePhoneNumber;
    private String email;

    @OneToMany
    @Builder.Default
    private Set<Location> locations = new HashSet<>();
}
