package com.awd.conman.entity.enums;

// This ENUM is not needed (i think)
public enum ERole {
    ROLE_SYSTEM_ADMIN,
    ROLE_OWNER,
    ROLE_FOREMAN,
    ROLE_EMPLOYEE,
    ROLE_CLIENT
}
