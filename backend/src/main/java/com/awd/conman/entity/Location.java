package com.awd.conman.entity;


import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SuperBuilder
public class Location extends BaseEntity {
  @OneToMany(fetch = FetchType.LAZY)
  @Builder.Default
  private Set<Label> labels = new HashSet<>();

  private String name;

  @NotNull
  @Column(name = "address_line_1", nullable = false)
  private String addressLine1;

  @Column(name = "address_line_2")
  private String addressLine2;

  @NotNull
  @Column(name = "city", nullable = false)
  private String city;

  @NotNull
  @Column(name = "state", length = 2, nullable = false)
  private String state;

  @NotNull
  @Column(name = "zip_code", length = 10, nullable = false)
  private String zipCode;

  @NotNull
  @Column(name = "country", length = 3, nullable = false)
  private String country;
}
