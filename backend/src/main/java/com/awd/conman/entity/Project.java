package com.awd.conman.entity;

import com.awd.conman.entity.enums.EProjectStatus;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SuperBuilder
public class Project extends BaseEntity {
    private String name;

    private String description;

    @Enumerated(EnumType.STRING)
    private EProjectStatus status;

    private Date startDate;

    private Date endDate;

    @OneToOne(fetch = FetchType.LAZY)
    private User createdBy;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id")
    private Client client;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "project")
    @Builder.Default
    private Set<TimeSegment> timeSegments = new HashSet<>();

    @Builder.Default
    private double totalHours = 0;

    @OneToMany(fetch = FetchType.LAZY)
    @Builder.Default
    private Set<Label> labels = new HashSet<>();

    @OneToMany
    @Builder.Default
    private Set<Location> locations = new HashSet<>();
}
