package com.awd.conman.entity.enums;

public enum EProjectStatus {
    READY, COMPLETED, CANCELED, IN_PROGRESS, WAITING_ON_OTHER, WAITING_ON_CUSTOMER
}
