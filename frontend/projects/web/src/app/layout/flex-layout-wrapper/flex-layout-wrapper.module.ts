import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutWrapperComponent } from './flex-layout-wrapper.component';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [FlexLayoutWrapperComponent],
  exports: [FlexLayoutWrapperComponent],
  imports: [CommonModule, FlexLayoutModule],
})
export class FlexLayoutWrapperModule {}
