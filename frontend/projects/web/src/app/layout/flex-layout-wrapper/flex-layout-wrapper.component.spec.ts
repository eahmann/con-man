import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlexLayoutWrapperComponent } from './flex-layout-wrapper.component';

describe('FlexLayoutWrapperComponent', () => {
  let component: FlexLayoutWrapperComponent;
  let fixture: ComponentFixture<FlexLayoutWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlexLayoutWrapperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlexLayoutWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
