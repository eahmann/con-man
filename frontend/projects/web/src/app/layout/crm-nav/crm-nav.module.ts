import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrmNavComponent } from './crm-nav.component';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { OverlayModule } from '@angular/cdk/overlay';
import { CrmToolbarContentComponent } from '@layout/crm-nav/crm-toolbar-content';
import { FlexLayoutWrapperModule } from '@layout/flex-layout-wrapper';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatExpansionModule } from '@angular/material/expansion';

@NgModule({
  declarations: [CrmNavComponent, CrmToolbarContentComponent],
  imports: [
    CommonModule,
    RouterModule,
    FlexLayoutModule,
    OverlayModule,
    FlexLayoutWrapperModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatExpansionModule,
  ],
})
export class CrmNavModule {}
