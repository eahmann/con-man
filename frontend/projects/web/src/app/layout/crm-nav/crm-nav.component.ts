import {
  AfterContentInit,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatSidenav } from '@angular/material/sidenav';
import { DeviceDetectorService } from 'ngx-device-detector';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-crm-nav',
  templateUrl: './crm-nav.component.html',
  styleUrls: ['./crm-nav.component.scss'],
})
export class CrmNavComponent implements OnDestroy, OnInit, AfterContentInit {
  @ViewChild('snav') sidenav: MatSidenav | undefined;
  isExpanded = true;
  isShowing = true;
  u: string;
  mobileQuery: MediaQueryList;

  private mobileQueryListener: () => void;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private deviceService: DeviceDetectorService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this.mobileQueryListener);
  }

  ngOnInit(): void {}

  ngAfterContentInit(): void {}

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this.mobileQueryListener);
  }

  isSubLinkActive(path: string): boolean {
    return this.router.routerState.snapshot.url.includes(path);
  }

  onToggleNav(): void {
    if (this.deviceService.isMobile() || this.mobileQuery.matches) {
      this.sidenav.toggle();
      this.isShowing = true;
      this.isExpanded = true;
    } else {
      this.isShowing = !this.isShowing;
      this.isExpanded = !this.isExpanded;
    }
  }

  mouseenter(): void {
    if (this.deviceService.isMobile()) {
      this.isShowing = true;
    }
    if (!this.isExpanded && this.deviceService.isDesktop()) {
      this.isShowing = true;
    }
    if (!this.isExpanded && this.deviceService.isTablet()) {
      this.isShowing = false;
    }
  }

  mouseleave(): void {
    if (!this.isExpanded) {
      this.isShowing = false;
    }
  }
}
