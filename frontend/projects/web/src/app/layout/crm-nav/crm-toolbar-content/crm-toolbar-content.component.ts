import { Component, OnInit } from '@angular/core';
import { User } from '@core/models';
import { Router } from '@angular/router';
import { AuthService, UserService } from '@core/services';

@Component({
  selector: 'app-crm-toolbar-content',
  templateUrl: './crm-toolbar-content.component.html',
  styleUrls: ['./crm-toolbar-content.component.scss'],
})
export class CrmToolbarContentComponent implements OnInit {
  user: User;
  /*  menuItems: MenuItem[] = [
    {
      label: 'Logout',
      set: 'fad',
      icon: 'fa-sign-out',
      class: '',
      path: 'auth/logout',
    },
  ];*/

  constructor(
    public router: Router,
    private userService: UserService,
    public authService: AuthService
  ) {}

  ngOnInit(): void {
    //this.userService.getSelf().subscribe();
    this.userService.user.subscribe((user) => {
      this.user = user;
    });
  }

  logout() {
    this.authService.logout();
  }
}
