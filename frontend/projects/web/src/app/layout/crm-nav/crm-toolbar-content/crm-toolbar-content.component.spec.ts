import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrmToolbarContentComponent } from './crm-toolbar-content.component';

describe('CrmToolbarContentComponent', () => {
  let component: CrmToolbarContentComponent;
  let fixture: ComponentFixture<CrmToolbarContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrmToolbarContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrmToolbarContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
