import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MarketingNavComponent} from './marketing-nav.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {RouterModule} from '@angular/router';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {FlexLayoutWrapperModule} from '@layout/flex-layout-wrapper';

@NgModule({
  declarations: [MarketingNavComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    RouterModule,
    MatToolbarModule,
    MatButtonModule,
    FlexLayoutWrapperModule,
  ],
})
export class MarketingNavModule {}
