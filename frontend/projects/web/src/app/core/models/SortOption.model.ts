export interface SortOption {
  value: string;
  name: string;
}
