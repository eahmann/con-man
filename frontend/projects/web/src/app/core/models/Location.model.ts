import { Base } from '@core/models/Base.model';

export interface Location extends Base {
  addressLine1: string;
  addressLine2: string;
  city: string;
  state: string;
  zipCode: string;
  country: string;
}
