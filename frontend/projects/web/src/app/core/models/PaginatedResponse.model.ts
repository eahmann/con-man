export interface PaginatedResponse<T> {
  totalItems: number;
  currentPage: number;
  totalPages: number;
  sortBy: string[];
  items: T;
}
