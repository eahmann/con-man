export interface ContactUsRequest {
  name: string;
  email: string;
  message: string;
}
