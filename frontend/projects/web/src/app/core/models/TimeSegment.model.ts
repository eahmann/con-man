export interface TimeSegment {
  hours: number;
  date: Date;
  createdBy: string;
  comment: string;
  id: string;
  version: number;
  createdDate: Date;
  lastModifiedDate: Date;
  employeeId: string;
  employeeName: string;
}
