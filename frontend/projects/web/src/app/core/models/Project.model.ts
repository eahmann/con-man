import {TimeSegment} from '@core/models/TimeSegment.model';
import {Label} from '@core/models/Label.model';
import {EProjectStatus} from '@core/models/enum/EProjectStatus';
import {Base} from '@core/models/Base.model';
import {Client} from '@core/models/Client.model';

export interface Project extends Base {
  name: string;
  description: string;
  startDate: Date;
  endDate: Date;
  status: EProjectStatus;
  createdById: string;
  client: Client;
  timeSegments: TimeSegment[];
  labels: Label[];
  totalHours: number;
}
