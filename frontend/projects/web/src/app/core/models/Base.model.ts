export interface Base {
  id: string;
  version: number;
  createdDate: Date;
  lastModifiedDate: Date;
}
