import {Profile} from '@core/models/Profile.model';
import {Project} from '@core/models/Project.model';
import {Base} from '@core/models/Base.model';

export interface Client extends Base {
  profile: Profile;
  projects: Project[];
}
