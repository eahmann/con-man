import { Base, Location } from '@core/models';
import { EProfileType } from '@core/models/enum/EProfileType';

export interface Profile extends Base {
  profileType: EProfileType;
  firstName: string;
  lastName: string;
  businessName: string;
  locations: Location[];
}
