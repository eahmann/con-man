import {Profile} from '@core/models/Profile.model';
import {Base} from '@core/models/Base.model';

export interface User extends Base {
  username: string;
  email: string;
  profile: Profile;
}
