export enum EProfileType {
  USER = 'USER',
  CLIENT = 'CLIENT',
}

export const EProfileTypeMap = {
  [EProfileType.USER]: 'User',
  [EProfileType.CLIENT]: 'Client',
};
