export enum EProjectStatus {
  READY = 'READY',
  IN_PROGRESS = 'IN_PROGRESS',
  COMPLETED = 'COMPLETED',
  WAITING_ON_OTHER = 'WAITING_ON_OTHER',
  WAITING_ON_CUSTOMER = 'WAITING_ON_CUSTOMER',
  CANCELED = 'CANCELED',
}

export const EProjectStatusMap = {
  [EProjectStatus.READY]: 'Ready',
  [EProjectStatus.IN_PROGRESS]: 'In progress',
  [EProjectStatus.COMPLETED]: 'Completed',
  [EProjectStatus.WAITING_ON_OTHER]: 'Waiting on other',
  [EProjectStatus.WAITING_ON_CUSTOMER]: 'Waiting on customer',
  [EProjectStatus.CANCELED]: 'Canceled',
};
