import { ELabelType } from '@core/models/enum/ELabelType';

export interface Label {
  id: string;
  name: string;
  labelType: ELabelType;
}
