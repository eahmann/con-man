export interface Sort {
  sortBy: string;
  direction: string;
}
