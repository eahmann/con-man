import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Project } from '@core/models/Project.model';
import { map } from 'rxjs/operators';
import { PaginatedResponse } from '@core/models/PaginatedResponse.model';

const baseUrl = `${environment.apiUrl}/projects`;

@Injectable({
  providedIn: 'root',
})
export class ProjectService {
  private projectSubject: BehaviorSubject<Project>;
  public project: Observable<Project>;

  private projectsSearchPageSubject: BehaviorSubject<
    PaginatedResponse<Project[]>
  >;
  public projectsSearchPage: Observable<PaginatedResponse<Project[]>>;

  constructor(private router: Router, private http: HttpClient) {
    this.projectSubject = new BehaviorSubject<Project>(null);
    this.project = this.projectSubject.asObservable();

    this.projectsSearchPageSubject = new BehaviorSubject<
      PaginatedResponse<Project[]>
    >(null);
    this.projectsSearchPage = this.projectsSearchPageSubject.asObservable();
  }

  public get projectValue(): Project {
    return this.projectSubject.value;
  }

  getAll(params: any): Observable<any> {
    return this.http.get<PaginatedResponse<Project[]>>(baseUrl, { params });
  }

  /*  getAll(params: any = {page: 1}): Observable<any> {
    return this.http
      .get<PaginatedResponse<Project[]>>(baseUrl, {params})
      .pipe(
        map((res: PaginatedResponse<Project[]>) => {
          this.projectsSearchPageSubject.next(res);
        })
      );
  }*/

  getById(id: string): Observable<any> {
    return this.http.get<Project>(`${baseUrl}/${id}`).pipe(
      map((res: Project) => {
        this.projectSubject.next(res);
      })
    );
  }

  create(project: Project): Observable<any> {
    return this.http.post(baseUrl, project);
  }

  update(id: string, project: Project): Observable<any> {
    return this.http.put<Project>(`${baseUrl}/${id}`, project).pipe(
      map((res: Project) => {
        if (res.id == this.projectValue.id) {
          this.projectSubject.next(res);
        }
      })
    );
  }
}
