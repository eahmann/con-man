import {Injectable} from '@angular/core';
import {environment} from '@env/environment';
import {BehaviorSubject, Observable} from 'rxjs';
import {TimeSegment} from '@core/models/TimeSegment.model';
import {HttpClient} from '@angular/common/http';

const baseUrl = `${environment.apiUrl}/time`;

@Injectable({
  providedIn: 'root',
})
export class TimeService {
  private timeSubject: BehaviorSubject<TimeSegment>;
  public timeSegment: Observable<TimeSegment>;

  constructor(private http: HttpClient) {
    this.timeSubject = new BehaviorSubject<TimeSegment>(null);
    this.timeSegment = this.timeSubject.asObservable();
  }

  public get timeSegmentValue(): TimeSegment {
    return this.timeSubject.value;
  }

  getAll(): Observable<any> {
    return this.http.get(baseUrl);
  }

  save(projectId: string, timeSegment: TimeSegment): Observable<any> {
    return this.http.post(`${baseUrl}/${projectId}`, timeSegment);
  }
}
