import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '@core/models';
import { HttpClient } from '@angular/common/http';

const baseUrl = `${environment.apiUrl}/users`;

@Injectable({
  providedIn: 'root',
})
export class UserService {
  public userSubject: BehaviorSubject<User>;
  public user: Observable<User>;

  constructor(private http: HttpClient) {
    this.userSubject = new BehaviorSubject<User>(null);
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): User {
    return this.userSubject.value;
  }

  getUserObs(): Observable<User> {
    return this.user;
  }

  getSelf(): Observable<any> {
    return this.http.get<User>(`${baseUrl}/self`).pipe(
      map((user) => {
        this.userSubject.next(user);
      })
    );
  }

  getUsersByRole(role: string): Observable<any> {
    return this.http.get(`${baseUrl}/role/${role}`);
  }
}
