import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Client } from '@core/models';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

const baseUrl = `${environment.apiUrl}/clients`;

@Injectable({
  providedIn: 'root',
})
export class ClientService {
  private clientSubject: BehaviorSubject<Client>;
  public client: Observable<Client>;

  constructor(private router: Router, private http: HttpClient) {
    this.clientSubject = new BehaviorSubject<Client>(null);
    this.client = this.clientSubject.asObservable();
  }

  public get clientValue(): Client {
    return this.clientSubject.value;
  }

  getAll(): Observable<Client[]> {
    return this.http.get<Client[]>(baseUrl);
  }

  create(client: Client): Observable<any> {
    return this.http.post(baseUrl, client);
  }
}
