import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Profile } from '@core/models';

const baseUrl = `${environment.apiUrl}/profiles`;

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  private profileSubject: BehaviorSubject<Profile>;
  public profile: Observable<Profile>;

  constructor(private router: Router, private http: HttpClient) {
    this.profileSubject = new BehaviorSubject<Profile>(null);
    this.profile = this.profileSubject.asObservable();
  }

  public get profileValue(): Profile {
    return this.profileSubject.value;
  }

  getAll(): Observable<Profile[]> {
    return this.http.get<Profile[]>(baseUrl);
  }

  create(profile: Profile): Observable<any> {
    return this.http.post(baseUrl, profile);
  }
}
