import { Injectable } from '@angular/core';
import { LoginRequest, RegisterRequest } from '@core/models';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { Router } from '@angular/router';
import { UserService } from '@core/services/user.service';
import { tap } from 'rxjs/operators';

const baseUrl = `${environment.apiUrl}/auth`;

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  isLoggedIn = false;

  constructor(
    private http: HttpClient,
    private userService: UserService,
    private router: Router
  ) {}

  register(registerRequestPayload: RegisterRequest): Observable<any> {
    return this.http.post(`${baseUrl}/signup`, registerRequestPayload);
  }

  login(loginRequestPayload: LoginRequest): Observable<any> {
    return this.http.post(`${baseUrl}/login`, loginRequestPayload).pipe(
      tap(() => {
        this.isLoggedIn = true;
        this.refresh().subscribe();
        console.log('started refresh timer at login');
        if (!this.userService.userValue) {
          this.userService.getSelf().subscribe();
          console.log('No userValue found, calling getSelf at login');
        }
      })
    );
  }

  logout(): void {
    // Clear cookies (api sets cookies as expired) and remove refresh token from db
    this.http.get(`${baseUrl}/logout`).subscribe();
    // Remove user observable
    this.userService.userSubject.next(null);
    this.isLoggedIn = false;
    this.stopRefreshTokenTimer();
    this.router.navigate(['/auth']);
    console.log('Redirect by logout');
  }

  refresh(): Observable<any> {
    return this.http.get(`${baseUrl}/refresh`).pipe(
      tap({
        next: () => {
          this.isLoggedIn = true;
          this.startRefreshTokenTimer();
          console.log('started refresh timer at refresh');
          if (!this.userService.userValue) {
            this.userService.getSelf().subscribe();
            console.log('No userValue found, calling getSelf at refresh');
          }
        },
        error: (error) => {
          console.log('on error', error.message);
        },
      })
    );
  }

  private refreshTokenTimeout: any;

  public startRefreshTokenTimer() {
    // set a timeout to refresh the token a minute before it expires
    const timeout = new Date();
    timeout.setMilliseconds(timeout.getMilliseconds() + 60000);
    this.refreshTokenTimeout = setTimeout(
      () => {
        console.log('Timer expired. Attempting to refresh token');
        this.refresh().pipe().subscribe();
      },

      timeout.getMilliseconds() + 60000
    );
  }

  public stopRefreshTokenTimer() {
    clearTimeout(this.refreshTokenTimeout);
  }
}
