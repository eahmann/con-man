import {Injectable} from '@angular/core';
import {environment} from '@env/environment';
import {BehaviorSubject, Observable} from 'rxjs';
import {Label} from '@core/models/Label.model';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';

const baseUrl = `${environment.apiUrl}/labels`;

@Injectable({
  providedIn: 'root',
})
export class LabelService {
  private labelSubject: BehaviorSubject<Label>;
  public label: Observable<Label>;

  constructor(private router: Router, private http: HttpClient) {
    this.labelSubject = new BehaviorSubject<Label>(null);
    this.label = this.labelSubject.asObservable();
  }

  public get labelValue(): Label {
    return this.labelSubject.value;
  }

  getAll(): Observable<any> {
    return this.http.get(baseUrl);
  }
}
