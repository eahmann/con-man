import { SortOption } from '@core/models/SortOption.model';
import { Sort } from '@core/models/Sort.model';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-sort-menu',
  templateUrl: './sort-menu.component.html',
  styleUrls: ['./sort-menu.component.scss'],
})
export class SortMenuComponent implements OnInit {
  @Input() sortOptions: Array<SortOption>;
  @Input() defaultIndex: number = 0;

  @Output() onSortChange = new EventEmitter<Sort>();

  sort: Sort;
  sortBy: SortOption;
  sortDirection: boolean;

  get sortDisplay(): string {
    return this.sortBy.name;
  }

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    // Default sort option
    this.sortBy = this.sortOptions[this.defaultIndex];

    // Initial sort
    this.sort = {
      sortBy: this.sortBy.value,
      direction: 'asc',
    };

    // Use query params when set
    this.route.queryParams.subscribe((params) => {
      if (params['sortBy']) {
        this.sortOptions.forEach((option) => {
          if (option.value == params['sortBy']) {
            this.sortBy = option;
          }
        });
      }
    });
  }

  onSortBy(sortSelection: SortOption): void {
    this.sort.sortBy = sortSelection.value;

    // Reset sort direction
    this.sortDirection = false;
    this.sort.direction = 'asc';

    this.onChange();
  }

  onToggleSortDirection(): void {
    // Flip flag
    this.sortDirection = !this.sortDirection;
    // Assign direction to sort
    this.sort.direction = this.sortDirection ? 'desc' : 'asc';
    this.onChange();
  }

  onChange(): void {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: this.sort,
      queryParamsHandling: 'merge',
    });
    this.onSortChange.emit(this.sort);
  }
}
