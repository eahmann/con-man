import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectStatusSelectDialogComponent } from './project-status-select-dialog.component';
import { ProjectStatusSelectModule } from '@core/components/form-controls/project-status-select';
import { MatDialogModule } from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [ProjectStatusSelectDialogComponent],
  imports: [
    CommonModule,
    ProjectStatusSelectModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatButtonModule,
  ],
})
export class ProjectStatusSelectDialogModule {}
