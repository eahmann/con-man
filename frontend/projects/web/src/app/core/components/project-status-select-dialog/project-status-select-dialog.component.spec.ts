import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectStatusSelectDialogComponent } from './project-status-select-dialog.component';

describe('ProjectStatusSelectDialogComponent', () => {
  let component: ProjectStatusSelectDialogComponent;
  let fixture: ComponentFixture<ProjectStatusSelectDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProjectStatusSelectDialogComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectStatusSelectDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
