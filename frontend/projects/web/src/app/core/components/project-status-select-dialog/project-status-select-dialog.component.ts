import { Component, Inject, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Project } from '@core/models/Project.model';
import { ProjectService } from '@core/services';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-project-status-select-dialog',
  templateUrl: './project-status-select-dialog.component.html',
  styleUrls: ['./project-status-select-dialog.component.scss'],
})
export class ProjectStatusSelectDialogComponent implements OnInit {
  statusForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private projectService: ProjectService,
    @Inject(MAT_DIALOG_DATA) public data: Project
  ) {}

  get form(): { [p: string]: AbstractControl } {
    return this.statusForm.controls;
  }

  get statusControl(): FormControl {
    return this.form['status'] as FormControl;
  }

  ngOnInit(): void {
    this.statusForm = this.formBuilder.group({
      status: ['', Validators.required],
    });
    this.statusForm.patchValue(this.data);
  }

  onSubmit(): void {
    this.projectService
      .update(this.data.id, this.statusForm.value)
      .pipe(first())
      .subscribe();
  }
}
