import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GoBackHeaderComponent } from './go-back-header.component';
import { GoBackButtonComponent } from '@core/components/go-back-header/go-back-button';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [GoBackHeaderComponent, GoBackButtonComponent],
  imports: [CommonModule, MatIconModule, MatButtonModule, FlexLayoutModule],
  exports: [GoBackHeaderComponent],
})
export class GoBackHeaderModule {}
