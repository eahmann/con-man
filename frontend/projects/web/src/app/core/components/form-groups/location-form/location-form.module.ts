import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { LabelSelectModule } from '@core/components/form-controls/label-select';
import { LocationFormComponent } from '@core/components/form-groups/location-form/location-form.component';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [LocationFormComponent],
  imports: [
    CommonModule,
    FlexModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    LabelSelectModule,
    MatButtonModule,
  ],
  exports: [LocationFormComponent],
})
export class LocationFormModule {}
