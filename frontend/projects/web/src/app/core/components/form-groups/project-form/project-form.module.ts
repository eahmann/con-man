import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectFormComponent } from './project-form.component';
import { LabelSelectModule } from '@core/components/form-controls/label-select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { ClientAutoselectModule } from '@core/components/form-controls/client-autoselect/client-autoselect.module';
import { ProjectStatusSelectModule } from '@core/components/form-controls/project-status-select';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [ProjectFormComponent],
  imports: [
    CommonModule,
    LabelSelectModule,
    MatDatepickerModule,
    MatInputModule,
    MatCardModule,
    ClientAutoselectModule,
    ProjectStatusSelectModule,
    ReactiveFormsModule,
    FlexModule,
    MatButtonModule,
  ],
})
export class ProjectFormModule {}
