import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LabelSelectComponent } from '@core/components/form-controls/label-select';
import { ELabelType } from '@core/models/enum/ELabelType';

@Component({
  selector: 'app-location-form',
  templateUrl: './location-form.component.html',
  styleUrls: ['./location-form.component.scss'],
})
export class LocationFormComponent implements OnInit {
  @ViewChild(LabelSelectComponent, { static: true })
  public labelSelectComponent: LabelSelectComponent;
  locationForm: FormGroup;
  numLocations = 1;
  labelType = ELabelType;

  get form() {
    return this.locationForm.controls;
  }
  get locations(): FormArray {
    return this.form.locations as FormArray;
  }

  get locationsFormGroups(): FormGroup[] {
    return this.locations.controls as FormGroup[];
  }

  constructor(private formBuilder: FormBuilder) {
    this.locationForm = this.formBuilder.group({
      locations: this.formBuilder.array([]),
    });
    this.generateLocationForms();
  }

  ngOnInit(): void {}

  locationFormGroup(): FormGroup {
    return this.formBuilder.group({
      labels: [[]],
      addressLine1: ['', Validators.required],
      addressLine2: [''],
      city: ['', Validators.required],
      state: ['', Validators.required],
      zipCode: ['', Validators.required],
      country: ['US', Validators.required],
    });
  }

  generateLocationForms(): void {
    for (let i = this.locations.length; i < this.numLocations; i++) {
      this.locations.push(this.locationFormGroup());
    }
  }

  addLocation() {
    this.numLocations++;
    this.generateLocationForms();
  }
}
