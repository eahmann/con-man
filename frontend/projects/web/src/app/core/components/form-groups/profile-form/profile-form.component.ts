import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { EProfileType } from '@core/models/enum/EProfileType';
import { LocationFormComponent } from '@core/components/form-groups/location-form/location-form.component';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss'],
})
export class ProfileFormComponent implements OnInit {
  @Input() profileType: EProfileType;
  @Output() formReady: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  @ViewChild(LocationFormComponent, { static: true })
  public locationFormComponent: LocationFormComponent;

  profileForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.profileForm = this.formBuilder.group({
      profileType: this.profileType,
      id: 'undefined',
      firstName: [''],
      lastName: [''],
      businessName: [''],
      locations: this.locationFormComponent.locations,
    });

    this.formReady.emit(this.profileForm);
  }

  public addFormControl(name: string, formArray: FormArray): void {
    this.profileForm.addControl(name, formArray);
  }
}
