import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { LabelSelectComponent } from '@core/components/form-controls/label-select/label-select.component';

@NgModule({
  declarations: [LabelSelectComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatSelectModule,
  ],
  exports: [LabelSelectComponent],
})
export class LabelSelectModule {}
