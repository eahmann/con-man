import { Component, Input, OnInit } from '@angular/core';
import { UserService } from '@core/services';
import { User } from '@core/models';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { first, map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-user-autoselect',
  templateUrl: './user-autoselect.component.html',
  styleUrls: ['./user-autoselect.component.scss'],
})
export class UserAutoselectComponent implements OnInit {
  @Input() control: FormControl;
  @Input() role: string;

  options: Array<User>;
  filteredOptions: Observable<User[]>;
  currentUser: User;

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.userService
      .getUsersByRole(this.role)
      .pipe(first())
      .subscribe((options) => {
        this.options = options;
        // Set initial value to logged in user
        this.control.setValue(this.options[0].id);
        // Display filtered options
        this.filteredOptions = this.control.valueChanges.pipe(
          startWith(''),
          map((value) =>
            typeof value === 'string'
              ? this._filter(value)
              : this.options.slice()
          )
        );
      });

    // Subscribe to logged in user for setting initial value
    this.userService.user.subscribe((user) => {
      this.currentUser = user;
    });
  }

  displayFn(id: string): string {
    if (!id) return '';
    let index = this.options.findIndex((option) => option.id === id);
    return index === -1
      ? ''
      : this.options[index].profile.firstName +
          ' ' +
          this.options[index].profile.lastName;
  }

  private _filter(value: string): User[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(
      (option) =>
        option.profile.firstName.toLowerCase().includes(filterValue) ||
        option.profile.lastName.toLowerCase().includes(filterValue)
    );
  }
}
