import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientAutoselectComponent } from './client-autoselect.component';

describe('CustomerAutoselectComponent', () => {
  let component: ClientAutoselectComponent;
  let fixture: ComponentFixture<ClientAutoselectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClientAutoselectComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientAutoselectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
