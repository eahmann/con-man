import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {LabelService} from '@core/services/label.service';
import {Label} from '@core/models/Label.model';

@Component({
  selector: 'app-label-select',
  templateUrl: './label-select.component.html',
  styleUrls: ['./label-select.component.scss'],
})
export class LabelSelectComponent implements OnInit {
  @Input() labelType: string;
  @Input() control: FormControl;
  labels: Array<Label> = [];

  constructor(private labelService: LabelService) {}

  ngOnInit(): void {
    this.labelService.getAll().subscribe((labels) => {
      this.labels = labels;
    });
  }
}
