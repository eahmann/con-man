import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { ProjectStatusSelectComponent } from '@core/components/form-controls/project-status-select/project-status-select.component';

@NgModule({
  declarations: [ProjectStatusSelectComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatOptionModule,
    MatSelectModule,
  ],
  exports: [ProjectStatusSelectComponent],
})
export class ProjectStatusSelectModule {}
