import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { EProjectStatus, EProjectStatusMap } from '@core/models';

@Component({
  selector: 'app-project-status-select',
  templateUrl: './project-status-select.component.html',
  styleUrls: ['./project-status-select.component.scss'],
})
export class ProjectStatusSelectComponent implements OnInit {
  @Input() control: FormControl;
  eProjectStatusMap = EProjectStatusMap;
  eProjectStatus = EProjectStatus;

  constructor() {}

  ngOnInit(): void {}
}
