import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { first, map, startWith } from 'rxjs/operators';

import { Client } from '@core/models';
import { ClientService } from '@core/services/client.service';

@Component({
  selector: 'app-customer-autoselect',
  templateUrl: './client-autoselect.component.html',
  styleUrls: ['./client-autoselect.component.scss'],
})
export class ClientAutoselectComponent implements OnInit {
  @Input() control: FormControl;
  options: Array<Client>;
  filteredOptions: Observable<Client[]>;

  constructor(private clientService: ClientService) {}

  ngOnInit(): void {
    this.clientService
      .getAll()
      .pipe(first())
      .subscribe((response) => {
        this.options = response;
        this.filteredOptions = this.control.valueChanges.pipe(
          startWith(''),
          map((value) =>
            typeof value === 'string'
              ? this._filter(value)
              : this.options.slice()
          )
        );
        console.log(this.options);
      });
  }

  displayFn(option: Client): string {
    return option
      ? `${option.profile.firstName} ${option.profile.lastName}`
      : undefined;
  }

  private _filter(value: string): Client[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(
      (option) =>
        option.profile.firstName.toLowerCase().includes(filterValue) ||
        option.profile.lastName.toLowerCase().includes(filterValue)
    );
  }
}
