import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAutoselectComponent } from './user-autoselect.component';

describe('UserAutoselectComponent', () => {
  let component: UserAutoselectComponent;
  let fixture: ComponentFixture<UserAutoselectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UserAutoselectComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAutoselectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
