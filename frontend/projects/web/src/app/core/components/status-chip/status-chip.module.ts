import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusChipComponent } from '@core/components/status-chip/status-chip.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatChipsModule } from '@angular/material/chips';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [StatusChipComponent],
  imports: [CommonModule, MatChipsModule, MatDialogModule, MatButtonModule],
  exports: [StatusChipComponent],
})
export class StatusChipModule {}
