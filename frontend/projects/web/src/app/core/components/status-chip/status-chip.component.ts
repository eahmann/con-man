import { Component, Input, OnInit } from '@angular/core';
import { EProjectStatus, EProjectStatusMap } from '@core/models';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ProjectStatusSelectDialogComponent } from '@core/components/project-status-select-dialog';

@Component({
  selector: 'app-status-chip',
  templateUrl: './status-chip.component.html',
  styleUrls: ['./status-chip.component.scss'],
})
export class StatusChipComponent implements OnInit {
  @Input('status') status: EProjectStatus;
  @Input('projectId') projectId: string;
  @Input('allowChange') allowChange: boolean;
  eProjectStatusMap = EProjectStatusMap;

  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {}

  openDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      id: this.projectId,
      status: this.status,
    };

    this.dialog.open(ProjectStatusSelectDialogComponent, dialogConfig);
  }

  none() {}
}
