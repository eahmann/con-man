import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { EMPTY, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService, UserService } from '@core/services';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private userService: UserService,
    private authService: AuthService
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((err) => {
        // 403 response and user logged in, try getting a new token
        if ([403].includes(err.status) && this.userService.userValue) {
          this.authService.refresh().subscribe();
        }
        // 500 response from refresh api and user logged in (failed refresh)
        if (
          [500].includes(err.status) &&
          request.url.endsWith('refresh') &&
          this.userService.userValue
        ) {
          // 500 response returned from api, kick user out
          this.authService.logout();
        }

        /*        const error = (err && err.error && err.error.message) || err.statusText;
        console.error(err);*/
        return EMPTY;
      })
    );
  }
}
