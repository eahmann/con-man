import { AuthService, UserService } from '@core/services';

export function appInitializer(
  authService: AuthService,
  userService: UserService
): () => Promise<any> {
  return () =>
    new Promise((resolve) => {
      authService.refresh().subscribe().add(resolve);
    });
}
