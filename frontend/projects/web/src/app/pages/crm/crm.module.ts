import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrmRoutingModule } from './crm-routing.module';
import { DashboardComponent } from './dashboard';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FlexLayoutWrapperModule } from '@layout/flex-layout-wrapper';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    RouterModule,
    CrmRoutingModule,
    FlexLayoutModule,
    FlexLayoutWrapperModule,
    MatCardModule,
    MatButtonModule,
  ],
})
export class CrmModule {}
