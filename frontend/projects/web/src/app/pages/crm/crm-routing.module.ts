import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard';
import { PageNotFoundComponent } from '../error/page-not-found';
import { AuthGuard } from '../../core/helpers';

const crmProjectModule = () =>
  import('./projects').then((x) => x.ProjectsModule);

const crmLeadsModule = () => import('./leads').then((x) => x.LeadsModule);
const crmTimeModule = () => import('./time').then((x) => x.TimeModule);
const crmClientsModule = () => import('./clients').then((x) => x.ClientsModule);

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
      {
        path: 'projects',
        loadChildren: crmProjectModule,
      },
      {
        path: 'leads',
        loadChildren: crmLeadsModule,
      },
      {
        path: 'time',
        loadChildren: crmTimeModule,
      },
      {
        path: 'clients',
        loadChildren: crmClientsModule,
      },
      {
        path: '**',
        component: PageNotFoundComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrmRoutingModule {}
