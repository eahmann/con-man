import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientCreateComponent } from '@pages/crm/clients/client-create/client-create.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'overview',
  },
  {
    path: 'overview',
    component: null,
  },
  {
    path: 'create',
    component: ClientCreateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClientsRoutingModule {}
