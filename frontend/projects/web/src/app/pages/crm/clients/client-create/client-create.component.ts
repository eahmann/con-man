import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EProfileType } from '@core/models/enum/EProfileType';
import { ClientService } from '@core/services/client.service';

@Component({
  selector: 'app-client-create',
  templateUrl: './client-create.component.html',
  styleUrls: ['./client-create.component.scss'],
})
export class ClientCreateComponent implements OnInit {
  clientForm: FormGroup;
  eProfileType = EProfileType;

  constructor(
    private formBuilder: FormBuilder,
    private clientService: ClientService
  ) {}

  ngOnInit(): void {
    this.clientForm = this.formBuilder.group({});
  }

  public addFormControl(name: string, formGroup: FormGroup): void {
    this.clientForm.addControl(name, formGroup);
  }

  onSubmit() {
    console.log(this.clientForm.value);
    this.clientService.create(this.clientForm.value).subscribe();
  }
}
