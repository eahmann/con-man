import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientsRoutingModule } from './clients-routing.module';
import { ClientCreateComponent } from './client-create/client-create.component';
import { ProfileFormModule } from '@core/components/form-groups/profile-form';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { LocationFormModule } from '@core/components/form-groups/location-form/location-form.module';

@NgModule({
  declarations: [ClientCreateComponent],
  imports: [
    CommonModule,
    ClientsRoutingModule,
    ProfileFormModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCardModule,
    LocationFormModule,
  ],
})
export class ClientsModule {}
