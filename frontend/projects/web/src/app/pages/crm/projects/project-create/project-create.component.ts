import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ProjectService } from '@core/services';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-project-create',
  templateUrl: './project-create.component.html',
  styleUrls: ['./project-create.component.scss'],
})
export class ProjectCreateComponent implements OnInit {
  projectForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private projectService: ProjectService
  ) {}

  get form(): { [p: string]: AbstractControl } {
    return this.projectForm.controls;
  }
  get statusControl(): FormControl {
    return this.form['status'] as FormControl;
  }
  get labelsControl(): FormControl {
    return this.form['labels'] as FormControl;
  }
  get clientControl(): FormControl {
    return this.form['client'] as FormControl;
  }

  ngOnInit(): void {
    this.projectForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      startDate: [Date.now(), Validators.required],
      endDate: [''],
      status: ['', Validators.required],
      client: ['', Validators.required],
      labels: [[]],
    });
  }

  onSubmit(): void {
    this.projectService
      .create(this.projectForm.value)
      .pipe(first())
      .subscribe((res) => {
        console.log(res);
      });
  }
}
