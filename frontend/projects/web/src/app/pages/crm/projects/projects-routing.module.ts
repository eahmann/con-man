import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectsOverviewComponent } from '@pages/crm/projects/projects-overview/projects-overview.component';
import { ProjectViewComponent } from '@pages/crm/projects/project-view';
import { ProjectSearchComponent } from '@pages/crm/projects/project-search';
import { ProjectAddEditComponent } from '@pages/crm/projects/project-add-edit';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'overview',
    pathMatch: 'full',
  },
  {
    path: 'overview',
    component: ProjectsOverviewComponent,
  },
  {
    path: 'search',
    component: ProjectSearchComponent,
  },
  {
    path: 'add',
    component: ProjectAddEditComponent,
    data: { reuseRoute: true },
  },
  {
    path: 'edit/:id',
    component: ProjectAddEditComponent,
  },

  {
    path: 'view/:id',
    component: ProjectViewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectsRoutingModule {}
