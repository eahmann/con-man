import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { ProjectService } from '@core/services';
import { BehaviorSubject, merge, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { Project } from '@core/models/Project.model';
import { SortOption } from '@core/models/SortOption.model';
import { Sort } from '@core/models/Sort.model';

@Component({
  selector: 'app-project-search',
  templateUrl: './project-search.component.html',
  styleUrls: ['./project-search.component.scss'],
})
export class ProjectSearchComponent implements AfterViewInit {
  pageSizeOptions: number[] = [5, 10, 25, 100];
  pageSize = 8;
  data: Project[];
  search: string;

  projects$: Project[];

  resultsLength = 0;
  isLoadingResults = true;
  isRefreshing = false;
  refresh = new BehaviorSubject<boolean>(false);

  sortOptions: Array<SortOption> = [
    { value: 'name', name: 'Name' },
    { value: 'description', name: 'Description' },
    { value: 'createdDate', name: 'Created Date' },
    { value: 'totalHours', name: 'Total hours worked' },
  ];
  sortSelection = new BehaviorSubject<Sort>({
    sortBy: this.sortOptions[0].value,
    direction: 'asc',
  });

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private projectService: ProjectService) {}

  sortChange(sort: Sort) {
    this.sortSelection.next(sort);
  }

  onRefresh() {
    this.isRefreshing = true;
    this.refresh.next(!this.refresh.value);
  }

  ngAfterViewInit() {
    // If the user changes the sort order, reset back to the first page.
    this.sortSelection.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sortSelection, this.paginator.page, this.refresh)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          const params = this.getRequestParams(
            this.search,
            this.paginator.pageIndex,
            this.paginator.pageSize,
            [
              this.sortSelection.getValue().sortBy +
                ',' +
                this.sortSelection.getValue().direction,
            ]
          );
          return this.projectService.getAll(params);
        }),
        map((data) => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRefreshing = false;
          this.resultsLength = data.totalItems;

          return data.items;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          return observableOf([]);
        })
      )
      .subscribe((data) => (this.data = data));
  }

  getRequestParams(
    search: string,
    page: number,
    size: number,
    sort: string[]
  ): any {
    let params: any = {};
    if (search) {
      params[`search`] = search;
    }
    if (page) {
      params[`page`] = page;
    }
    if (size) {
      params[`size`] = size;
    }
    if (sort) {
      params[`sort`] = sort;
    }
    return params;
  }
}
