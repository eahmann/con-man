import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Project } from '@core/models/Project.model';
import { ProjectService } from '@core/services';
import { TimeSegment } from '@core/models/TimeSegment.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-project-view',
  templateUrl: './project-view.component.html',
  styleUrls: ['./project-view.component.scss'],
})
export class ProjectViewComponent implements OnInit {
  id: string;
  project: Project;
  selectedTimeSegment: TimeSegment;

  project$: Observable<Project>;

  constructor(
    private route: ActivatedRoute,
    private projectService: ProjectService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.id = params.id;
      this.projectService.getById(params.id).subscribe();
      this.projectService.project.subscribe(
        (project) => (this.project = project)
      );
    });
  }

  onEdit(segment: TimeSegment) {
    this.selectedTimeSegment = segment;
  }

  tabLoadTimes: Date[] = [];

  getTimeLoaded(index: number) {
    if (!this.tabLoadTimes[index]) {
      this.tabLoadTimes[index] = new Date();
    }

    return this.tabLoadTimes[index];
  }
}
