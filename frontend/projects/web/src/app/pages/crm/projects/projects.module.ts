import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectViewComponent } from './project-view';
import { ProjectAddEditComponent } from './project-add-edit';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ProjectsOverviewComponent } from './projects-overview/projects-overview.component';
import { ProjectCreateComponent } from '@pages/crm/projects/project-create';
import { ReactiveFormsModule } from '@angular/forms';
import { LabelSelectModule } from '@core/components/form-controls/label-select';
import { TimeModule } from '@pages/crm/time';
import { StatusChipModule } from '@core/components/status-chip/status-chip.module';
import { SortMenuModule } from '@core/components/sort-menu';
import { ProjectSettingsMenuComponent } from './project-view/project-settings-menu/project-settings-menu.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatDialogModule } from '@angular/material/dialog';
import { ProjectSearchComponent } from '@pages/crm/projects/project-search';
import { ClientAutoselectModule } from '@core/components/form-controls/client-autoselect/client-autoselect.module';
import { ProjectStatusSelectModule } from '@core/components/form-controls/project-status-select';
import { GoBackHeaderModule } from '@core/components/go-back-header/go-back-header.module';

@NgModule({
  declarations: [
    ProjectSearchComponent,
    ProjectViewComponent,
    ProjectAddEditComponent,
    ProjectsOverviewComponent,
    ProjectCreateComponent,
    ProjectSettingsMenuComponent,
  ],
  imports: [
    CommonModule,
    ProjectsRoutingModule,
    NgxDatatableModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    ClientAutoselectModule,
    ProjectStatusSelectModule,
    LabelSelectModule,
    TimeModule,
    StatusChipModule,
    SortMenuModule,
    MatTabsModule,
    MatCardModule,
    MatListModule,
    MatIconModule,
    MatButtonToggleModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatMenuModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatDialogModule,
    GoBackHeaderModule,
  ],
})
export class ProjectsModule {}
