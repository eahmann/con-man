import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectSettingsMenuComponent } from './project-settings-menu.component';

describe('ProjectSettingsMenuComponent', () => {
  let component: ProjectSettingsMenuComponent;
  let fixture: ComponentFixture<ProjectSettingsMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProjectSettingsMenuComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectSettingsMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
