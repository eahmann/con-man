import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ProjectService } from '@core/services';
import { first } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-project-add-edit',
  templateUrl: './project-add-edit.component.html',
  styleUrls: ['./project-add-edit.component.scss'],
})
export class ProjectAddEditComponent implements OnInit {
  projectForm: FormGroup;
  id: string;
  isAddMode: boolean;
  loading = false;
  submitted = false;
  touched = false;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private projectService: ProjectService
  ) {}

  get form(): { [p: string]: AbstractControl } {
    return this.projectForm.controls;
  }
  get statusControl(): FormControl {
    return this.form['status'] as FormControl;
  }
  get labelsControl(): FormControl {
    return this.form['labels'] as FormControl;
  }
  get clientControl(): FormControl {
    return this.form['client'] as FormControl;
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;

    this.projectForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      startDate: [Date.now(), Validators.required],
      endDate: [''],
      status: ['', Validators.required],
      client: ['', Validators.required],
      labels: [[]],
    });

    // Populate form from server
    if (!this.isAddMode) {
      this.loading = true;
      this.projectService
        .getById(this.id)
        .pipe(first())
        .subscribe((x) => {
          this.loading = false;
          console.log(x);
          this.projectForm.patchValue(this.projectService.projectValue);
          this.touched = false;
        });
    }

    this.projectForm.valueChanges.subscribe((x) => {
      this.touched = true;
      this.submitted = false;
    });
  }

  onSubmit(): void {
    this.loading = true;
    this.submitted = false;
    if (this.isAddMode) {
      this.projectService
        .create(this.projectForm.value)
        .pipe(first())
        .subscribe((res) => {
          this.loading = false;
          this.submitted = true;
          console.log(res);
        });
    } else {
      this.projectService
        .update(this.id, this.projectForm.value)
        .pipe(first())
        .subscribe((res) => {
          this.loading = false;
          this.submitted = true;
          console.log(res);
        });
    }
  }

  onReset(): void {
    this.projectForm.reset();
  }
}
