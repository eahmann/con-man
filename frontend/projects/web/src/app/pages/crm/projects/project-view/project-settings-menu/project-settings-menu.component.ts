import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-project-settings-menu',
  templateUrl: './project-settings-menu.component.html',
  styleUrls: ['./project-settings-menu.component.scss'],
})
export class ProjectSettingsMenuComponent implements OnInit {
  @Input() projectId: string;

  constructor() {}

  ngOnInit(): void {}
}
