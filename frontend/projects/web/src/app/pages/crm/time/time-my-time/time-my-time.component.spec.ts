import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeMyTimeComponent } from './time-my-time.component';

describe('TimeMyTimeComponent', () => {
  let component: TimeMyTimeComponent;
  let fixture: ComponentFixture<TimeMyTimeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TimeMyTimeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeMyTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
