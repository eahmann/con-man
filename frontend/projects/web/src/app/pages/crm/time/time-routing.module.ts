import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TimeOverviewComponent } from '@pages/crm/time/time-overview/time-overview.component';
import { TimeCreateComponent } from '@pages/crm/time/time-create/time-create.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'overview',
    pathMatch: 'full',
  },
  {
    path: 'overview',
    component: TimeOverviewComponent,
  },
  {
    path: 'create',
    component: TimeCreateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TimeRoutingModule {}
