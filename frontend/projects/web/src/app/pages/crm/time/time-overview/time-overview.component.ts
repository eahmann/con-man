import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-time-overview',
  templateUrl: './time-overview.component.html',
  styleUrls: ['./time-overview.component.scss'],
})
export class TimeOverviewComponent implements OnInit {
  week: Date;

  constructor() {}

  ngOnInit(): void {
    this.week = new Date();
  }
}
