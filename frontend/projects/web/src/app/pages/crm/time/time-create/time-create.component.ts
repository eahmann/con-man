import { Component, Input, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { TimeService } from '@core/services/time.service';
import { first } from 'rxjs/operators';
import { TimeSegment } from '@core/models/TimeSegment.model';

@Component({
  selector: 'app-time-create',
  templateUrl: './time-create.component.html',
  styleUrls: ['./time-create.component.scss'],
})
export class TimeCreateComponent implements OnInit {
  @Input() projectId: string;

  private _timeSegment: TimeSegment;
  @Input() set timeSegment(value: TimeSegment) {
    this._timeSegment = value;
    if (this.formGroup && value) {
      this.formGroup.controls['id'].setValue(value.id);
      this.formGroup.controls['hours'].setValue(value.hours);
      this.formGroup.controls['date'].setValue(value.date);
    }
  }

  get timeSegment(): TimeSegment {
    return this._timeSegment;
  }

  formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private timeService: TimeService
  ) {}
  get form(): { [p: string]: AbstractControl } {
    return this.formGroup.controls;
  }
  get userControl(): FormControl {
    return this.form['employeeId'] as FormControl;
  }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      id: [null],
      comment: [''],
      hours: ['', Validators.required],
      date: ['', Validators.required],
      employeeId: [''],
    });
    this.formGroup.controls['date'].setValue(new Date());
    if (this.timeSegment) {
      this.formGroup.setValue(this.timeSegment);
    }
  }

  onSubmit(): void {
    this.timeService
      .save(this.projectId, this.formGroup.value)
      .pipe(first())
      .subscribe();
  }

  onCancelEdit(): void {
    this.timeSegment = null;
    this.formGroup.controls['id'].setValue(null);
    this.formGroup.controls['hours'].setValue('');
    this.formGroup.controls['date'].setValue(new Date());
  }
}
