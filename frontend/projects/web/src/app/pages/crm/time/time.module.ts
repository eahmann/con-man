import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TimeRoutingModule } from './time-routing.module';
import { TimeMyTimeComponent } from './time-my-time/time-my-time.component';
import { TimeOverviewComponent } from './time-overview/time-overview.component';
import { TimeCreateComponent } from './time-create/time-create.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { UserAutoselectModule } from '@core/components/form-controls/user-autoselect/user-autoselect.module';

@NgModule({
  declarations: [
    TimeMyTimeComponent,
    TimeOverviewComponent,
    TimeCreateComponent,
  ],
  imports: [
    CommonModule,
    TimeRoutingModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    UserAutoselectModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
  ],
  providers: [MatNativeDateModule],
  exports: [TimeCreateComponent],
})
export class TimeModule {}
