import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LeadsOverviewComponent} from '@pages/crm/leads/leads-overview/leads-overview.component';
import {LeadsListComponent} from '@pages/crm/leads/leads-list';
import {LeadAddEditComponent} from '@pages/crm/leads/lead-add-edit';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'overview',
    pathMatch: 'full',
  },
  {
    path: 'overview',
    component: LeadsOverviewComponent,
  },
  {
    path: 'list',
    component: LeadsListComponent,
  },
  {
    path: 'add',
    component: LeadAddEditComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LeadsRoutingModule {}
