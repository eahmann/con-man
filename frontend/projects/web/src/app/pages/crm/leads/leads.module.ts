import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LeadsRoutingModule} from './leads-routing.module';
import {LeadsListComponent} from './leads-list';

import {LeadViewComponent} from './lead-view';
import {LeadAddEditComponent} from './lead-add-edit';
import {LeadsOverviewComponent} from './leads-overview/leads-overview.component';

@NgModule({
  declarations: [
    LeadsListComponent,
    LeadViewComponent,
    LeadAddEditComponent,
    LeadsOverviewComponent,
  ],
  imports: [CommonModule, LeadsRoutingModule],
})
export class LeadsModule {}
