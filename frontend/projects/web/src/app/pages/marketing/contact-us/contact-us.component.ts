import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss'],
})
export class ContactUsComponent implements OnInit {
  contactUsForm: FormGroup;
  name$: Observable<string>;
  isSendSuccess: boolean | null = null;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.contactUsForm = this.formBuilder.group({
      name: [''],
      email: ['', Validators.email],
      message: [''],
    });
  }

  onSubmit() {
    this.isSendSuccess = true;
  }
}
