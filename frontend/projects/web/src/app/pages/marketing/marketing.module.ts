import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarketingRoutingModule } from './marketing-routing.module';
import { LandingComponent } from '@pages/marketing/landing';
import { MatCardModule } from '@angular/material/card';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FlexModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { ContactUsComponent } from '@pages/marketing/contact-us';

@NgModule({
  declarations: [LandingComponent, ContactUsComponent],
  imports: [
    CommonModule,
    MarketingRoutingModule,
    MatCardModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    FlexModule,
    MatButtonModule,
  ],
})
export class MarketingModule {}
