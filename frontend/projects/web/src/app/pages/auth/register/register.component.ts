import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../core/services';
import { RegisterRequest } from '../../../core/models';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  // @ts-ignore
  signupForm: FormGroup;
  // @ts-ignore
  registerRequestPayload: RegisterRequest;

  constructor(private authService: AuthService) {
    this.registerRequestPayload = {
      username: '',
      email: '',
      password: '',
    };
  }

  ngOnInit(): void {
    this.signupForm = new FormGroup({
      username: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
    });
  }

  signup(): void {
    this.registerRequestPayload.username = this.signupForm.get(
      'username'
    ).value;
    this.registerRequestPayload.email = this.signupForm.get('email').value;
    this.registerRequestPayload.password = this.signupForm.get(
      'password'
    ).value;

    this.authService.register(this.registerRequestPayload).subscribe(
      () => {
        console.log('Signup Successful');
      },
      () => {
        console.log('Signup Failed');
      }
    );
  }
}
