import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginRequest } from '../../../core/models';
import { AuthService } from '../../../core/services';
import { UserService } from '../../../core/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup | undefined;
  loginRequestPayload: LoginRequest;
  isError: boolean;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.loginRequestPayload = {
      username: 'eric',
      password: 'password1',
    };
  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }

  login(): void {
    this.loginRequestPayload.username = this.loginForm.get('username').value;
    this.loginRequestPayload.password = this.loginForm.get('password').value;

    this.authService
      .login(this.loginRequestPayload)
      .pipe(first())
      .subscribe({
        next: () => {
          //this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          const returnUrl =
            this.route.snapshot.queryParams['returnUrl'] || '/crm';
          this.router.navigateByUrl(returnUrl);
        },
        error: (err) => {
          console.log(err);
        },
      });
  }
}
