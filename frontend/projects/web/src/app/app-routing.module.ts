import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MarketingNavComponent } from '@layout/marketing-nav';
import { CrmNavComponent } from '@layout/crm-nav';
import { AuthNavComponent } from '@layout/auth-nav';
import { PageNotFoundComponent } from '@pages/error/page-not-found';
import { AuthGuard } from '@core/helpers';

const marketingModule = () =>
  import('@pages/marketing').then((x) => x.MarketingModule);
const crmModule = () => import('@pages/crm').then((x) => x.CrmModule);
const authModule = () => import('@pages/auth').then((x) => x.AuthModule);

const routes: Routes = [
  {
    path: '',
    component: MarketingNavComponent,
    loadChildren: marketingModule,
  },
  {
    path: 'crm',
    component: CrmNavComponent,
    canActivate: [AuthGuard],
    loadChildren: crmModule,
  },
  {
    path: 'auth',
    component: AuthNavComponent,
    loadChildren: authModule,
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
